import { Dimensions, Image, ImageBackground, SafeAreaView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import * as Animatable from 'react-native-animatable';
import { getData } from '../../Services/LocalStorage'
import { useDispatch, useSelector } from 'react-redux'
import { set_Contry, set_subscriptionDetails, store_setAccessToken, store_setUserdetails } from '../../Store/Reducers/CommonReducer'
import HelperFunctions from '../../Constants/HelperFunctions'
import { api_postWithToken, api_postWithoutToken } from '../../Services/Service'

const styles = Style()

const SplashScreen = (props) => {
  const isFocused = useIsFocused()
  const dispatch = useDispatch();

  const { access_token, user_details, subscriptionDetails } = useSelector(state => state.commonData);

  useEffect(() => {
    // api_fetchCOuntry()
    // == Localstorage data fetch
    checkLocalstorageData();
    
    // setTimeout(() => {
    //     // props.navigation.replace("HomeScreen")
    //     props.navigation.replace("IntroScreen")
    //     // props.navigation.replace("OtpScreen")


    // }, 1000);
  }, [])

  // useEffect(() => {
  //   // set_Contry
  //   api_fetchCOuntry()
  // })

  checkLocalstorageData =()=>{
    getData('access_token').then((accessResponse) => {
      if (accessResponse) {
        dispatch(store_setAccessToken(accessResponse));
        getData('user_details').then((response) => {
          if (response) {
            console.log("ABC ABC =======>",accessResponse);
            dispatch(store_setUserdetails(JSON.parse(response)))
            // setTimeout(() => {
            //   props.navigation.replace("IntroScreen")
            // }, 1000);
            api_fetch_my_subscription(accessResponse)
          } else {
            props.navigation.replace("IntroScreen")
          }
        })
          .catch((error1) => {
            console.log("error1 ===>", error1)
            HelperFunctions.showToastMsg(JSON.stringify(error1))
            props.navigation.replace("IntroScreen")
          })

      } else {
        props.navigation.replace("IntroScreen")
      }
    })
      .catch((error) => {
        console.log("error ===>", error)
        HelperFunctions.showToastMsg(JSON.stringify(error))
        props.navigation.replace("IntroScreen")
      })
  }


  const api_fetch_my_subscription = (userToken) => {
    console.log("usertojen : ",userToken)
    api_postWithToken('user/subscription/active-plan', {}, userToken).then(response => {
      // setShowloading(false)
      if (response.status == 'success') {
        // Object.keys(globalData).length === 0
        const sub_data = response.data;
        if (sub_data?.subscription) {
          console.log("sub_data?.subscription : ", sub_data?.subscription)
          // console.log("csdcbskdjbcsjkdc ", subscriptionDetails);
          dispatch(set_subscriptionDetails(sub_data?.subscription));
          // dispatch(set_subscriptionDetails(null));
          props.navigation.replace('HomeScreen')
          // props.navigation.replace('SubscriptionplanScreen')
        } else {
          console.log("console.print here")
          props.navigation.replace('SubscriptionplanScreen')
        }
      } else {
        // HelperFunctions.showToastMsg(response.message)
      }

    }).catch(error => {
    })
  }



  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <Animatable.View animation="zoomInUp">
        <Image style={{ width: Theme.windowWidth * 0.50, alignSelf: 'center', height: Theme.windowWidth * 0.50 }} source={AllSourcePath.LOCAL_IMAGES.login_prayLogo} />
      </Animatable.View>
    </SafeAreaView>

  )
}

export default SplashScreen

