import { Dimensions, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import { useDispatch, useSelector } from 'react-redux'
import { api_postWithoutToken } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'

const styles = Style()

const ForgotpassScreen = (props) => {
    const isFocused = useIsFocused()

    const [email, setEmail] = useState("")

    const [eyevalue, setEyevalue] = useState(true)
    const [showloading, setShowloading] = useState(false)
    const dispatch = useDispatch();

    const { access_token, user_details, subscriptionDetails } = useSelector(state => state.commonData);
  

    useEffect(() => {
        // setTimeout(() => {
        //     props.navigation.replace("HomeScreen")
        // }, 2000);
        // setTimeout(() => {
        //     setShowloading(false);
        // }, 5000);
    }, [])

    //Reset password function
    const func_resetPassword = () =>{
        if(email){
            // props.navigation.navigate('ResetpassScreen')
            call_resetPassword()
        }
    }

    // Call reset password API
    const call_resetPassword = () =>{
        setShowloading(true);
        let params={
            email:email
        }
        api_postWithoutToken('auth/password-reset',params)
        .then((response)=>{
            setShowloading(false)
            if(response.status == 'otpverification'){
                console.log("SUccess forgot password")
                HelperFunctions.showToastMsg(response?.message)
                // let otp_token = response?.data?.otp_token

                let optionParams={
                    otp_token:response?.data?.otp_token,
                    otp_email:email,
                    passwordReset_type:'user-resetpassword'
                }
                // props.navigation.push('OtpScreen',dataToSend)
                props.navigation.push('ResetpassScreen',optionParams)
            }else{
                HelperFunctions.showToastMsg(response?.message)
            }

            // if(success.status == 'success'){
            //     // this.global.presentToast(success.message, 5000)
            //   }else if(success.status == 'otpverification'){
                
            //     let successdata:any = success.data; 
            //     this.global.user_token = successdata.otp_token;
            //     console.log("Forgot password : ", this.global.user_token);
            //     this.global.passwordReset_type = 'user-resetpassword';
            //     this.route.navigate(['./reset-password'])
            //   }else{
            //     this.global.presentToast(success.message,null);
            //   }


        })
        .catch((error)=>{
            setShowloading(false)
            HelperFunctions.showToastMsg(JSON.stringify(error));
        })
    }
                                



    return (
        <ScreenLayout
            isScrollable={false}
            LeftComponent={''}
            // layoutBackground={{ backgroundColor:'#fff'}}
            IconColor={'#fff'}
            headerStyle={{backgroundColor:'#fff'}}
            hideCustomHeader={false}
            viewStyle={{ backgroundColor: 'white', }}
            customStatusbarColor={"#ff0000"}
            showLoading={showloading}
            hideLeftComponentView={false}
            leftComponentOnPress={()=>props.navigation.goBack()}
            // hideMiddleComponentView={true}
            hideRightComponentView={true}
            // rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
        >
            
            <View style={{ flex: 1 }}>
                {/* <Image
                    style={{
                        // alignSelf: 'center', 
                        // marginVertical: 20, 
                        objectFit: 'fit',
                        height: '100%', width: Dimensions.get('window').width,
                        opacity: 0.5
                    }}

                    source={AllSourcePath.LOCAL_IMAGES.background_one} /> */}
            </View>
            <ScrollView style={{ paddingHorizontal: 30, position: 'absolute', height: '100%', width: '100%' }}>

                <View style={{ flex: 1 }}>
                    <View style={{ height: 50 }}></View>
                    <View style={{}}>
                        <Image
                            style={{
                                width: Dimensions.get('window').width / 2.1,
                                alignSelf: 'center', marginVertical: 20,
                                height: Dimensions.get('window').width / 2.1,
                                // borderRadius: Dimensions.get('window').width / 3.1
                            }}
                            source={AllSourcePath.LOCAL_IMAGES.forgetpass_logo}
                        />
                    </View>
                    {/* <View>
                        <Text style={{ fontSize: 24, color: '#1D232A' }}> Sign forgot</Text>
                        <Text style={{ fontSize: 24, color: '#1D232A' }}> your account </Text>
                    </View> */}
                    <View style={{ paddingTop: 50 }}>
                        <EditTextComponent
                            style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                            placeholder={"Enter Registeered Email*"}
                            // iconName={eyevalue ? 'eye-off-outline' : 'eye-outline'}
                            // iconPress={() => { setEyevalue(!eyevalue) }}
                            // secureTextEntry={eyevalue ? true : false}
                            value={email}
                            onChangeText={(emailValue) => { setEmail(emailValue) }}

                        />
                    </View>

                    <View style={{ paddingTop: 40 }}>
                        <CustomButton
                            buttonText={"Reset Now"}
                            customIconName="arrow-up"
                            customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
                            onPress={() => { 
                                func_resetPassword()
                                // props.navigation.navigate('ResetpassScreen')
                            
                            }}
                        />
                    </View>
                </View>

            </ScrollView>

            {/* <View style={{ flexDirection: 'row', justifyContent: 'center', paddingVertical: 20 }}>
                <Text> New account? </Text>
                <Pressable onPress={() => {
                    props.navigation.replace("SignupScreen")
                }}>
                    <Text style={{ color: '#eb9800' }}>Registration</Text>
                </Pressable>
            </View> */}

        </ScreenLayout>

    )
}

export default ForgotpassScreen

