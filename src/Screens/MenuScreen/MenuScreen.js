import {
  Dimensions,
  Image,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  Linking,
  Share,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import AllSourcePath from '../../Constants/PathConfig';
import Theme from '../../Constants/Theme';
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon';
import Style from './Style';
import {useIsFocused} from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import {deleteData, getData} from '../../Services/LocalStorage';
import {useDispatch, useSelector} from 'react-redux';
import {
  set_subscriptionDetails,
  store_setAccessToken,
  store_setUserdetails,
} from '../../Store/Reducers/CommonReducer';
import HelperFunctions from '../../Constants/HelperFunctions';
import {api_postWithToken, api_postWithoutToken} from '../../Services/Service';
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';

const MenuScreen = props => {
  const isFocused = useIsFocused();
  const dispatch = useDispatch();

  // const [, setPassword] = useState("12345678")
  const {globalData, access_token, user_details, subscriptionDetails} =
    useSelector(state => state.commonData);
  const [logoutModalValue, setLogoutModalValue] = useState(false);
  const [showloading, setshowloading] = useState(false);

  const sidemenuList = [
    {name: 'Home', redirect_url: 'HomeScreen'},
    {name: 'Subscriptions', redirect_url: 'SubscriptionplanScreen'},
    {name: 'My Subscriptions', redirect_url: 'MySubscriptionScreen'},
    {name: 'Payment History', redirect_url: 'PaymentHistoryScreen'},
    {name: 'About us', redirect_url: 'AboutUs'},
    {name: 'Privacy Policy', redirect_url: 'PrivacyScreen'},
    {name: 'Terms & Condition', redirect_url: 'TermsScreen'},
    {name: 'Share App', redirect_url: 'share'},
    {name: 'Logout', redirect_url: 'LoginScreen'},
    {name: 'Delete Account', redirect_url: ''},
  ];

  useEffect(() => {
    // console.log("Menu userDetails : ", user_details?.avatar);
    // if (user_details?.avatar) {
    // }
  }, [user_details]);

  // == Share app
  const onShare = async () => {
    try {
      const result = await Share.share({
        title: 'App link',
        message: 'Please install this app and enjoy , AppLink :',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      Alert.alert(error.message);
    }
  };

  const func_deleteAccount = () => {
    setshowloading(true);
    api_postWithToken('user/profile/delete', {}, access_token)
      .then(response => {
        setshowloading(false);
        if (response.status == 'success') {
          func_logout();
        } else {
          HelperFunctions.showToastMsg(response.message);
        }
      })
      .catch(error => {
        setshowloading(false);
        HelperFunctions.showToastMsg(error.message);
        func_logout();
      });
  };

  const func_logout = () => {
    console.log('logout function clicked');

    dispatch(store_setAccessToken(''));
    dispatch(store_setUserdetails({}));
    setLogoutModalValue(false);
    deleteData();
    setTimeout(() => {
      props.navigation.replace('LoginScreen');
    }, 1000);
  };

  const alert_deleteAccountConfirmation = () => {
    Alert.alert(
      'Delete Account!',
      'Are you sure? You want to delete your account?', // <- this part is optional, you can pass an empty string
      [
        {text: 'Cancel', onPress: () => {}},
        {
          text: 'Detele Account',
          onPress: () => {
            func_deleteAccount();
          },
        },
      ],
      {cancelable: false},
    );
  };

  const flatlist_showItem = item => {
    // console.log("sdcsd", item)
    return (
      <TouchableOpacity
        onPress={() => {
          console.log(item);
          if (item.item.name == 'Delete Account') {
            alert_deleteAccountConfirmation();
          } else {
            func_gotoScreen(item.item);
          }
        }}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingVertical: 15,
        }}>
        <View>
          <Text>{item?.item?.name}</Text>
        </View>
        <View>
          <Icon name="chevron-forward" />
        </View>
      </TouchableOpacity>
    );
  };

  const func_gotoScreen = item => {
    console.log('csdckjsdbcksd :', item);
    console.log(props.route.name);
    // if( props.route.name == "MenuScreen" ){

    // }

    if (item.name == 'Logout') {
      setLogoutModalValue(true);
    } else if (item.name == 'Share App') {
      onShare();
    } else {
      props.navigation.replace(item?.redirect_url);
    }
  };

  const showLogoutModal = () => {};

  return (
    <ScreenLayout
      isScrollable={false}
      LeftComponent={''}
      layoutBackground={{backgroundColor: '#fff'}}
      // headerStyle={{ backgroundColor: '#eb9800' }}
      hideCustomHeader={true}
      viewStyle={{backgroundColor: 'white', width: '100%'}}
      customStatusbarColor={'#ff0000'}
      showLoading={showloading}
      hideLeftComponentView={true}
      leftComponentOnPress={() => props.navigation.goBack()}>
      <View
        style={{
          height: 120,
          backgroundColor: '#F2F2F2',
          justifyContent: 'center',
          paddingHorizontal: 20,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                borderWidth: 0.5,
                height: 80,
                width: 80,
                borderRadius: 80,
              }}>
              {/* {user_details?.avatar ?  */}
              <Image
                source={{uri: user_details?.avatar}}
                style={{height: 80, width: 80, borderRadius: 80}}
              />
              {/* :null} */}
            </View>
            <View style={{justifyContent: 'center', paddingLeft: 15}}>
              <Text style={{fontSize: 16, fontWeight: '600', paddingBottom: 5}}>
                {user_details?.name}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate('EditprofileScreen');
                }}>
                <Text>View and edit profile</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => props.navigation.goBack()}>
              <Image
                source={require('../../assets/images/cross.png')}
                style={{
                  height: 24,
                  width: 24,
                  alignSelf: 'flex-end',
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>

      {/* <TouchableOpacity onPress={() => {func_logout()}}>
          <Text>Logout</Text>
        </TouchableOpacity> */}
      <View style={{paddingHorizontal: 25, flex: 1}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={sidemenuList}
          renderItem={flatlist_showItem}
        />
      </View>

      <View style={{height: '20%', justifyContent: 'center'}}>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <View style={{paddingHorizontal: 10}}>
            <Icon
              name="logo-facebook"
              size={22}
              onPress={() => {
                Linking.openURL('https://www.facebook.com/');
              }}
            />
          </View>

          <View style={{paddingHorizontal: 10}}>
            <Icon
              name="logo-instagram"
              size={22}
              onPress={() => {
                Linking.openURL('https://www.instagram.com/');
              }}
            />
          </View>

          <View style={{paddingHorizontal: 10}}>
            <Icon
              name="logo-twitter"
              size={22}
              onPress={() => {
                Linking.openURL('https://twitter.com/home?lang=en');
              }}
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingTop: 20,
          }}>
          <Text>Version- 1.0.0</Text>
        </View>
      </View>

      <View>
        <Modal
          animationIn={'fadeIn'}
          animationOut={'fadeOut'}
          isVisible={logoutModalValue}
          // animationType="slide"
          // transparent={true}
          onBackdropPress={() => {
            // setLogoutModalValue(false)
          }}>
          <View
            style={{
              // height: '30%',
              width: Dimensions.get('window').width - 120,
              marginTop: 'auto',
              marginBottom: Dimensions.get('window').height / 2.5,
              backgroundColor: 'white',
              alignSelf: 'center',
              borderRadius: 10,
              // marginHorizontal:20
            }}>
            <View
              style={{
                backgroundColor: '#E7E8E9',
                borderTopEndRadius: 10,
                borderTopLeftRadius: 10,
              }}>
              <Text
                style={{
                  fontSize: 16,
                  paddingVertical: 10,
                  letterSpacing: 1,
                  fontWeight: 500,
                  textAlign: 'center',
                }}>
                Logout
              </Text>
            </View>
            <View style={{}}>
              <Text
                style={{
                  fontSize: 14,
                  paddingTop: 20,
                  paddingHorizontal: 20,
                  letterSpacing: 1,
                }}>
                Are you sure, you want to logout?
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 20,
                paddingHorizontal: 20,
              }}>
              <View>
                <TouchableOpacity
                  onPress={() => {
                    setLogoutModalValue(false);
                  }}>
                  <Text style={{fontSize: 16, letterSpacing: 0.5}}>Cancel</Text>
                </TouchableOpacity>
              </View>

              <View>
                <TouchableOpacity
                  onPress={() => {
                    func_logout();
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 500,
                      letterSpacing: 0.5,
                      color: 'red',
                      textAlign: 'right',
                    }}>
                    Logout
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </ScreenLayout>
  );
};

export default MenuScreen;
