import { Dimensions, FlatList, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import Icon from 'react-native-vector-icons/Ionicons'

import Modal from "react-native-modal";
import { api_getWithoutToken, api_postWithoutToken } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'
import { useDispatch, useSelector } from 'react-redux'
import { setcommonglobalData } from '../../Store/Reducers/CommonReducer'

const styles = Style()

const SignupScreen = (props) => {
    const isFocused = useIsFocused()
    const dispatch = useDispatch()

    const [fullname, setFullname] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const [eyevalue, setEyevalue] = useState(true)
    const [showloading, setShowloading] = useState(false)
    
    const [age_id,setAge_id] = useState('')
    const [age_value,setAge_value] = useState('')
    const [showAgegroup, setShowAgegroup] = useState(false)

    const [showReigion, setShowReigion] = useState(false)
    const [reigion_id,setReigion_id] = useState('')
    const [reigion_value,setReigion_value] = useState('')
    

    const { globalData } = useSelector(state => state.commonData);


    


    useEffect(() => {
        console.log("globalData : ", globalData)
        // func_getglobalData()
        if(Object.keys(globalData).length === 0){
            func_getglobalData();
        }

    }, [])

    const func_getglobalData = () => {
        console.log("dskcskdj")
        setShowloading(true)
        api_getWithoutToken('auth/signup/master',{}).then(response=>{
            setShowloading(false)
            if(response.status=='success'){
                dispatch(setcommonglobalData(response.data))
            }else{
                HelperFunctions.showToastMsg(response.message)
            }
        
        }).catch(error=>{
            setShowloading(false)
            HelperFunctions.showToastMsg(error.message)
        })
    }
    // == Age group modal flatlist ====
    const flatlist_showAgegroups =(data) =>{
        console.log("data : ", data?.item?.id)
        return(
            <TouchableOpacity 
                style={{borderRadius:5,borderWidth:1,marginHorizontal:20,marginTop:10,borderColor:'#B8B8B8'}}
                onPress={()=>{
                    setAge_id(data?.item?.id)
                    setAge_value(data?.item?.name)
                    setTimeout(() => {
                        setShowAgegroup(false)
                    }, 200);
                }}
            >
                <Text style={{fontSize:16,padding:12}}>{data?.item?.name}</Text>
            </TouchableOpacity>
        )
    }

    // == Reigion modal flatlist ====
    const flatlist_showReigion =(data) =>{
        console.log("data : ", data?.item)
        return(
            <TouchableOpacity 
                style={{borderRadius:5,borderWidth:1,marginHorizontal:20,marginTop:10,borderColor:'#B8B8B8'}}
                onPress={()=>{
                    setReigion_id(data?.item?.id)
                    setReigion_value(data?.item?.name)
                    setTimeout(() => {
                        setShowReigion(false)
                    }, 200);
                }}
            >
                <Text style={{fontSize:16,padding:12}}>{data?.item?.name}</Text>
            </TouchableOpacity>
        )
    }

    // = Email validation ====
    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
            console.log("Email is Not Correct");
            setEmail(text)
            return false;
        }
        else {
            setEmail(text)
            console.log("Email is Correct");
            return true;
        }
        }

    // == function registration ===
    const registrationStart = () =>{
        if(!fullname){
            HelperFunctions.showToastMsg('Full name can not be empty');
        }else if(!email){
            HelperFunctions.showToastMsg('Email can not be empty')
        }else if(!reigion_id){
            HelperFunctions.showToastMsg('Reigion can not be emply')
        }else if(!age_id){
            HelperFunctions.showToastMsg('Age group can not be empty')
        }else if(!password){
            HelperFunctions.showToastMsg('Password can not be empty')
        }else{
            const isvalid = validate(email);
            if(isvalid == true){
                func_registration()
            }else{
                HelperFunctions.showToastMsg("Email format is incorrect")
            }
        }
    }

    const func_registration = () =>{
        let param={
            name:fullname,
            email:email,
            religion_id:reigion_id,
            age_group_id:age_id,
            password:password,
            password_confirmation:password
        }
        setShowloading(true);
        api_postWithoutToken('auth/signup',param)
        .then((response)=>{
            setShowloading(false);
            console.log("response: " + JSON.stringify(response))
            if(response.status == 'otpverification'){
                console.log("response?.data?.otp_token : ", response?.data?.otp_token);
                const dataToSend = { param1: response?.data?.otp_token };
                props.navigation.push('OtpScreen',dataToSend)
            }else{
                HelperFunctions.showToastMsg(response?.message)
            }
        })
        .catch((error)=>{
            setShowloading(false);
        })

    }



    



    return (
        <ScreenLayout
            isScrollable={false}
            LeftComponent={false}
            hideCustomHeader={true}
            viewStyle={{ backgroundColor: 'white', }}
            // layoutBackground={{ backgroundColor: '#ff0000' }}
            customStatusbarColor={"#ff0000"}
            showLoading={showloading}
        // hideLeftComponentView={true}
        // hideMiddleComponentView={true}
        // hideRightComponentView={true}
        // rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
        >

            <View style={{ flex: 1 }}>
                <Image
                    style={{
                        // alignSelf: 'center', 
                        // marginVertical: 20, 
                        // objectFit: 'fit',
                        height: '100%', width: Dimensions.get('window').width,
                        opacity: 0.5
                    }}

                    source={AllSourcePath.LOCAL_IMAGES.background_one} />
            </View>

            <View style={{paddingHorizontal: 30, position: 'absolute', width: '100%',height:Dimensions.get('window').height-150, marginBottom: '50%'}}>
                <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                 style={{  }}>

                    <View style={{ flex: 1 }}>
                        <View style={{ height: 30 }}></View>
                            <View style={{}}>
                                <Image
                                    style={{
                                        width: Dimensions.get('window').width / 3.1,
                                        alignSelf: 'center', marginVertical: 20,
                                        height: Dimensions.get('window').width / 3.1,
                                        borderRadius: Dimensions.get('window').width / 3.1
                                    }}
                                    source={AllSourcePath.LOCAL_IMAGES.login_prayLogo}
                                />
                            </View>
                            <View>
                                <Text style={{ fontSize: 24, color: '#1D232A' }}> Create</Text>
                                <Text style={{ fontSize: 24, color: '#1D232A' }}> your account </Text>
                            </View>
                            <View style={{ paddingTop: 30 }}>
                                <EditTextComponent
                                    style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                                    placeholder={"Full Name*"}
                                    // iconName={eyevalue ? 'eye-off-outline' : 'eye-outline'}
                                    // iconPress={() => { setEyevalue(!eyevalue) }}
                                    secureTextEntry={false}
                                    value={fullname}
                                    onChangeText={(textValue) => { setFullname(textValue) }}
                                />
                            </View>

                            <View style={{ paddingTop: 20 }}>
                                <EditTextComponent
                                    style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                                    placeholder={"Email*"}
                                    value={email}
                                    onChangeText={(textValue) => { setEmail(textValue) }}
                                />
                            </View>


                            <View style={{ paddingTop: 20 }}>
                                <TouchableOpacity 
                                    onPress={()=>{setShowReigion(true)}}
                                    style={{
                                        borderRadius: 4, flexDirection: 'row', borderColor: '#B8B8B8', borderWidth: 0.5,
                                        alignItems: 'center', paddingHorizontal: 10,
                                        backgroundColor: Theme.colors.inputFieldColor, height: 45
                                    }}
                                >
                                    <View style={{ flex: 1, }}>
                                        <Text style={{ opacity: 0.4 }}>{reigion_value ? reigion_value : 'Select Reigion*'}</Text>
                                    </View>
                                    <View>
                                        <Icon name={'caret-down'} size={16} style={{ paddingHorizontal: 10 }} />
                                    </View>

                                </TouchableOpacity>

                            </View>

                            <View style={{ paddingTop: 20 }}>
                                <TouchableOpacity 
                                    onPress={()=>{setShowAgegroup(true)}}
                                    style={{
                                        borderRadius: 4, flexDirection: 'row', borderColor: '#B8B8B8', borderWidth: 0.5,
                                        alignItems: 'center', paddingHorizontal: 10,
                                        backgroundColor: Theme.colors.inputFieldColor, height: 45
                                    }}
                                >
                                    <View style={{ flex: 1, }}>
                                        <Text style={{ opacity: 0.4 }}>{age_value ? age_value : 'Select Age Group*'}</Text>
                                    </View>
                                    <View>
                                        <Icon name={'caret-down'} size={16} style={{ paddingHorizontal: 10 }} />
                                    </View>

                                </TouchableOpacity>

                            </View>



                            <View style={{ paddingTop: 20 }}>
                                <EditTextComponent
                                    style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                                    placeholder={"Password*"}
                                    iconName={eyevalue ? 'eye-off-outline' : 'eye-outline'}
                                    iconPress={() => { setEyevalue(!eyevalue) }}
                                    secureTextEntry={eyevalue ? true : false}
                                    value={password}
                                    onChangeText={(textValue) => { setPassword(textValue) }}
                                />
                            </View>

                        <View style={{ paddingTop: 40 }}>
                            <CustomButton
                                buttonText={"Register Now"}
                                customIconName="arrow-up"
                                customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
                                onPress={() => { registrationStart() }}
                            />
                        </View>
                    </View>

                </ScrollView>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingVertical: 20 }}>
                <Text> Already have an account? </Text>
                <Pressable onPress={() => {
                    props.navigation.replace("LoginScreen")
                }}>
                    <Text style={{ color: '#eb9800' }}>Login</Text>
                </Pressable>
            </View>

            {/* Modal => Reigion Modal */}
            <View>
                <Modal isVisible={showReigion}
                    // animationType="slide"
                    // transparent={true}
                    onBackdropPress={() => {
                        setShowReigion(false)
                      }}
                >
                    {/* <View style={{ 
                        height: '40%',width:Dimensions.get('window').width,
                        marginTop: 'auto', marginBottom:-20,
                        backgroundColor:'white',alignSelf:'center' }}
                    >
                        <Text style={{fontSize:16,paddingVertical:15,paddingHorizontal:20,backgroundColor:'#E7E8E9',letterSpacing:1,fontWeight:500}}>Select Reigion*</Text>
                        <View style={{}}>
                            <FlatList
                            showsVerticalScrollIndicator={false}
                            data={globalData?.age_groups}
                            renderItem={flatlist_showAgegroups}
                            />
                        </View>
                    
                    </View> */}
                    <View style={{ 
                        height: '40%',width:Dimensions.get('window').width,
                        marginTop: 'auto', marginBottom:-20,
                        backgroundColor:'white',alignSelf:'center',
                        borderTopEndRadius:10,borderTopLeftRadius:10
                    
                    }}
                    >
                        <View style={{backgroundColor:'#E7E8E9',borderTopEndRadius:10,borderTopLeftRadius:10}}>
                            <Text 
                                style={{
                                    fontSize:16,paddingVertical:10,paddingHorizontal:20,
                                    letterSpacing:1,fontWeight:500,
                                    
                                }}
                            >
                                Select Reigion*
                            </Text>
                        </View>
                        <View style={{}}>
                            <FlatList
                            showsVerticalScrollIndicator={false}
                            data={globalData?.religions}
                            renderItem={flatlist_showReigion}
                            />
                        </View>     
                    </View>
                    
                </Modal>
            </View>

            {/* Modal => Age group Modal */}
            <View>
                <Modal isVisible={showAgegroup}
                    // animationType="slide"
                    // transparent={true}
                    onBackdropPress={() => {
                        setShowAgegroup(false)
                      }}
                >
                    <View style={{ 
                        height: '40%',width:Dimensions.get('window').width,
                        marginTop: 'auto', marginBottom:-20,
                        backgroundColor:'white',alignSelf:'center',
                        borderTopEndRadius:10,borderTopLeftRadius:10
                    
                    }}
                    >
                        <View style={{backgroundColor:'#E7E8E9',borderTopEndRadius:10,borderTopLeftRadius:10}}>
                            <Text 
                                style={{
                                    fontSize:16,paddingVertical:10,paddingHorizontal:20,
                                    letterSpacing:1,fontWeight:500,
                                    
                                }}
                            >
                                Select Age group*
                            </Text>
                        </View>
                        <View style={{}}>
                            <FlatList
                            showsVerticalScrollIndicator={false}
                            data={globalData?.age_groups}
                            renderItem={flatlist_showAgegroups}
                            />
                        </View>     
                    </View>

                </Modal>
            </View>



        </ScreenLayout>

    )
}

export default SignupScreen

