import { Dimensions, FlatList, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import Icon from 'react-native-vector-icons/Ionicons'

import Modal from "react-native-modal";
import { api_getWithoutToken, api_postWithToken, api_postWithoutToken, api_uploadImage } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'
import { useDispatch, useSelector } from 'react-redux'
import { setcommonglobalData, store_setUserdetails } from '../../Store/Reducers/CommonReducer'

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';


const styles = Style()

const EditprofileScreen = (props) => {
	const isFocused = useIsFocused()
	const dispatch = useDispatch()

	const [fullname, setFullname] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [eyevalue, setEyevalue] = useState(true)
	const [showloading, setShowloading] = useState(false)

	const [age_id, setAge_id] = useState('')
	const [age_value, setAge_value] = useState('')
	const [showAgegroup, setShowAgegroup] = useState(false)

	const [showReigion, setShowReigion] = useState(false)
	const [reigion_id, setReigion_id] = useState('')
	const [reigion_value, setReigion_value] = useState('')


	const { globalData, user_details, access_token } = useSelector(state => state.commonData);





	useEffect(() => {
		console.log("globalData : ", globalData)
		// func_getglobalData()
		if (Object.keys(globalData).length === 0) {
			func_getglobalData();
		}

		if (user_details?.religion) {
			setReigion_id(user_details?.religion?.id)
			setReigion_value(user_details?.religion?.name)
		}
		if (user_details?.agegroup) {
			setAge_id(user_details?.agegroup?.id)
			setAge_value(user_details?.agegroup?.name)
		}
		if (user_details?.name) {
			setFullname(user_details?.name)
		}
		if (user_details?.email) {
			setEmail(user_details?.email)
		}

	}, [user_details])

	const func_getglobalData = () => {
		console.log("dskcskdj")
		setShowloading(true)
		api_getWithoutToken('auth/signup/master', {}).then(response => {
			setShowloading(false)
			if (response.status == 'success') {
				dispatch(setcommonglobalData(response.data))
			} else {
				HelperFunctions.showToastMsg(response.message)
			}

		}).catch(error => {
			setShowloading(false)
			HelperFunctions.showToastMsg(error.message)
		})
	}
	// == Age group modal flatlist ====
	const flatlist_showAgegroups = (data) => {
		console.log("data : ", data?.item?.id)
		return (
			<TouchableOpacity
				style={{ borderRadius: 5, borderWidth: 1, marginHorizontal: 20, marginTop: 10, borderColor: '#B8B8B8' }}
				onPress={() => {
					setAge_id(data?.item?.id)
					setAge_value(data?.item?.name)
					setTimeout(() => {
						setShowAgegroup(false)
					}, 200);
				}}
			>
				<Text style={{ fontSize: 16, padding: 12 }}>{data?.item?.name}</Text>
			</TouchableOpacity>
		)
	}

	// == Reigion modal flatlist ====
	const flatlist_showReigion = (data) => {
		console.log("data : ", data?.item)
		return (
			<TouchableOpacity
				style={{ borderRadius: 5, borderWidth: 1, marginHorizontal: 20, marginTop: 10, borderColor: '#B8B8B8' }}
				onPress={() => {
					setReigion_id(data?.item?.id)
					setReigion_value(data?.item?.name)
					setTimeout(() => {
						setShowReigion(false)
					}, 200);
				}}
			>
				<Text style={{ fontSize: 16, padding: 12 }}>{data?.item?.name}</Text>
			</TouchableOpacity>
		)
	}

	// // = Email validation ====
	// validate = (text) => {
	//     console.log(text);
	//     let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
	//     if (reg.test(text) === false) {
	//         console.log("Email is Not Correct");
	//         setEmail(text)
	//         return false;
	//     }
	//     else {
	//         setEmail(text)
	//         console.log("Email is Correct");
	//         return true;
	//     }
	//     }

	// == function registration ===
	const updateStart = () => {
		if (!fullname) {
			HelperFunctions.showToastMsg('Full name can not be empty');
		} else {
			func_update()
		}
	}

	const func_update = () => {
		let param = {
			type: 'basic-details',
			name: fullname,
			email: email,
			religion_id: reigion_id,
			age_group_id: age_id
		}

		console.log(param)
		setShowloading(true);
		api_postWithToken('user/profile/update', param, access_token)
			.then((response) => {
				setShowloading(false);
				console.log("response: " + JSON.stringify(response))
				if (response.status == 'success') {
					let successdata = response?.data;
					console.log("successdata.user ==>", successdata.user);
					// HelperFunctions.showToastMsg(response?.message);
					// setData('user_details',JSON.stringify(successdata.user))
					dispatch(store_setUserdetails(successdata.user))


					// setTimeout(() => {
					console.log("After submit ==> ", user_details);
					// }, 5000);

				} else {
					HelperFunctions.showToastMsg(response?.message)
				}
			})
			.catch((error) => {
				setShowloading(false);
			})

	}

	const func_capturePic = () =>{

		// includeBase64:false
		launchImageLibrary( 
      {mediaType: 'photo',quality:0.1,
		},
		// base64
      ({didCancel, errorCode, errorMessage, assets}) => {
        if (errorCode) {
          HelperFunctions.showToastMsg(errorMessage);
        } else if (assets) {
          const form = new FormData();
          form.append('type', 'profile-picture');
          form.append('profile_picture', {
            uri: assets[0].uri,
            type: assets[0].type,
            name: assets[0].fileName,
          });

		// let form={
		// 	type:'profile-picture',
		// 	assets:assets
		// }
					// form.append('profile_picture','data:image/jpg;base64,'+assets[0].base64);

          uploadImageToServer(form);
        }
      },
    );
	}

	const uploadImageToServer = (form) => {
    // changeloadingState(true);
	// api_uploadImage('user/profile/update', form, access_token,'multipart/form-data')
    api_postWithToken('user/profile/update', form, access_token,'multipart/form-data')
      .then(response => {
				console.log("Image upload response :", response);
        // if (response.status == true) {

        //   // setProfileImage(response.data.profile_image);
        //   // dispatch(
        //   //   setExtraUserDetails({profile_image: response.data.profile_image}),
        //   // );
        // } else {
        //   HelperFunctions.showToastMsg(response.message);
        // }
      })
      .catch(error => {
		console.log("Image upload error :", error);
        HelperFunctions.showToastMsg(error.message);
       
      })
      // .finally((success) => {
			// 	console.log("Image upload response finally",JSON.stringify(success));
      //   // changeloadingState(false);
      // });
  };







	return (
		<ScreenLayout
			isScrollable={false}
			// LeftComponent={<Icon name='home' />}
			// hideCustomHeader={true}
			// viewStyle={{ backgroundColor: 'white', }}
			// layoutBackground={'#eb9800' }
			// IconColor={'#fff'}
			layoutBackground={'#eb9800'}
			IconColor={'#fff'}
			headerStyle={{ backgroundColor: '#eb9800' }}

			// layoutBackground={{ backgroundColor: '#ff0000' }}
			// customStatusbarColor={"#ff0000"}
			showLoading={showloading}
		hideLeftComponentView={false}
		leftComponentOnPress={()=>{props.navigation.replace('HomeScreen')}}
		
		// hideMiddleComponentView={true}
		hideRightComponentView={true}
		// rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
		LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Update Profile</Text>}
		>

			<View style={{ paddingHorizontal: 30, height: Dimensions.get('window').height }}>
				<ScrollView
					showsVerticalScrollIndicator={false}
					showsHorizontalScrollIndicator={false}
					style={{}}>

					<View style={{ flex: 1 }}>
						<TouchableOpacity
							onPress={() => {func_capturePic() }}
							style={{ height: 100, width: 100, borderWidth: 0.5, borderRadius: 100, alignSelf: 'center' }}>
							<Image source={{ uri: user_details?.avatar }} style={{ height: 100, width: 100, borderRadius: 100 }}
							/>
							<View style={{ height: 20, width: 20, backgroundColor: 'white', position: 'absolute', bottom: 5, right: 5, borderRadius: 20, borderColor: '#000', borderWidth: 0.5, alignItems: 'center' }}>
								{/* <View style={{justifyContent:'center',height:20,width:20,flexDirection:'column'}}> */}
								<Icon name="camera" style={{ height: 20, width: 20, paddingLeft: '20%', paddingTop: '16%' }} />
								{/* </View> */}
							</View>
						</TouchableOpacity>

						<View style={{ paddingTop: 30 }}>
							<EditTextComponent
								style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
								placeholder={"Full Name*"}
								// iconName={eyevalue ? 'eye-off-outline' : 'eye-outline'}
								// iconPress={() => { setEyevalue(!eyevalue) }}
								secureTextEntry={false}
								value={fullname}
								onChangeText={(textValue) => { setFullname(textValue) }}
							/>
						</View>

						<View style={{ paddingTop: 20 }}>
							<EditTextComponent
								style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
								placeholder={"Email*"}
								value={email}
								onChangeText={(textValue) => { setEmail(textValue) }}
								editable={false}
							/>
						</View>


						<View style={{ paddingTop: 20 }}>
							<TouchableOpacity
								onPress={() => { setShowReigion(true) }}
								style={{
									borderRadius: 4, flexDirection: 'row', borderColor: '#B8B8B8', borderWidth: 0.5,
									alignItems: 'center', paddingHorizontal: 10,
									backgroundColor: Theme.colors.inputFieldColor, height: 45
								}}
							>
								<View style={{ flex: 1, }}>
									<Text style={{ opacity: 0.4 }}>{reigion_value ? reigion_value : 'Select Reigion*'}</Text>
								</View>
								<View>
									<Icon name={'caret-down'} size={16} style={{ paddingHorizontal: 10 }} />
								</View>

							</TouchableOpacity>

						</View>

						<View style={{ paddingTop: 20 }}>
							<TouchableOpacity
								onPress={() => { setShowAgegroup(true) }}
								style={{
									borderRadius: 4, flexDirection: 'row', borderColor: '#B8B8B8', borderWidth: 0.5,
									alignItems: 'center', paddingHorizontal: 10,
									backgroundColor: Theme.colors.inputFieldColor, height: 45
								}}
							>
								<View style={{ flex: 1, }}>
									<Text style={{ opacity: 0.4 }}>{age_value ? age_value : 'Select Age Group*'}</Text>
								</View>
								<View>
									<Icon name={'caret-down'} size={16} style={{ paddingHorizontal: 10 }} />
								</View>

							</TouchableOpacity>

						</View>



						<View style={{ paddingTop: 40 }}>
							<CustomButton
								buttonText={"Update Now"}
								customIconName="arrow-up"
								customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
								onPress={() => { updateStart() }}
							/>
						</View>
					</View>

				</ScrollView>
			</View>

			{/* Modal => Reigion Modal */}
			<View>
				<Modal isVisible={showReigion}
					// animationType="slide"
					// transparent={true}
					onBackdropPress={() => {
						setShowReigion(false)
					}}
				>

					<View style={{
						height: '40%', width: Dimensions.get('window').width,
						marginTop: 'auto', marginBottom: -20,
						backgroundColor: 'white', alignSelf: 'center',
						borderTopEndRadius: 10, borderTopLeftRadius: 10

					}}
					>
						<View style={{ backgroundColor: '#E7E8E9', borderTopEndRadius: 10, borderTopLeftRadius: 10 }}>
							<Text
								style={{
									fontSize: 16, paddingVertical: 10, paddingHorizontal: 20,
									letterSpacing: 1, fontWeight: 500,

								}}
							>
								Select Reigion*
							</Text>
						</View>
						<View style={{}}>
							<FlatList
								showsVerticalScrollIndicator={false}
								data={globalData?.religions}
								renderItem={flatlist_showReigion}
							/>
						</View>
					</View>

				</Modal>
			</View>

			{/* Modal => Age group Modal */}
			<View>
				<Modal isVisible={showAgegroup}
					// animationType="slide"
					// transparent={true}
					onBackdropPress={() => {
						setShowAgegroup(false)
					}}
				>
					<View style={{
						height: '40%', width: Dimensions.get('window').width,
						marginTop: 'auto', marginBottom: -20,
						backgroundColor: 'white', alignSelf: 'center',
						borderTopEndRadius: 10, borderTopLeftRadius: 10

					}}
					>
						<View style={{ backgroundColor: '#E7E8E9', borderTopEndRadius: 10, borderTopLeftRadius: 10 }}>
							<Text
								style={{
									fontSize: 16, paddingVertical: 10, paddingHorizontal: 20,
									letterSpacing: 1, fontWeight: 500,

								}}
							>
								Select Age group*
							</Text>
						</View>
						<View style={{}}>
							<FlatList
								showsVerticalScrollIndicator={false}
								data={globalData?.age_groups}
								renderItem={flatlist_showAgegroups}
							/>
						</View>
					</View>

				</Modal>
			</View>



		</ScreenLayout>

	)
}

export default EditprofileScreen

