import { Dimensions, FlatList, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import Icon from 'react-native-vector-icons/Ionicons'

import Modal from "react-native-modal";
import { api_getWithoutToken, api_postWithoutToken } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'
import { useDispatch, useSelector } from 'react-redux'
import { setcommonglobalData } from '../../Store/Reducers/CommonReducer'
import OtpInput from '../../Components/OtpInput/OtpInputComponent'
import BackIcon from '../../assets/icons/BackIcon'



const styles = Style()

const OtpScreen = (props) => {
	const isFocused = useIsFocused()
	const dispatch = useDispatch()
	const [showloading, setShowloading] = useState(false)

	const { globalData } = useSelector(state => state.commonData);
	const [otp, setOtp] = useState('');
	const [paramToken, setParamToken] = useState('');



	const { param1 } = props?.route?.params;

	useEffect(() => {
		if (param1) {
			console.log("OTP token : ", param1)
			setParamToken(param1)
		}
	}, [param1])

	// OTP complete function
	const handleOtpComplete = (enteredOtp) => {
		// setOtp(enteredOtp);
		// Alert.alert('Entered OTP:', enteredOtp);
		console.log("enteredOtp:", enteredOtp);
		setOtp(enteredOtp);
		api_submitOtp(enteredOtp)
	};

	//Resend OTP function
	const func_resendOtp = () => {
		setShowloading(true);
		let params = {
			verification_token: param1,
		}
		api_postWithoutToken('otp/resend', params)
			.then((response) => {
				setShowloading(false)
				if (response.status == 'success') {
					HelperFunctions.showToastMsg(response?.message)
				} else {
					HelperFunctions.showToastMsg(response?.message)
				}
			})
			.catch((error) => {
				setShowloading(false)
				HelperFunctions.showToastMsg(JSON.stringify(error))
			})
	}

	//== Submit OTP function
	const api_submitOtp = (otpValue) => {
		if (otpValue) {
			setShowloading(true)
			let params = {
				type: 'user-register',
				//   type:this.global.passwordReset_type,
				verification_token: param1,
				otp: otpValue
			}

			api_postWithoutToken('otp/verify', params)
				.then((response) => {
					setShowloading(false)
					console.log("WORKING==>", JSON.stringify(response))
					if (response.status == 'success') {
						props.navigation.replace('LoginScreen');
					} else {
						HelperFunctions.showToastMsg(response?.message)
					}
				})
				.catch((error) => {
					setShowloading(false)
					console.log("otp/verify error : ", JSON.stringify(error));
					HelperFunctions.showToastMsg(JSON.stringify(error))
				})



		}
	}



	return (
		<ScreenLayout
			isScrollable={false}
			LeftComponent={false}
			hideCustomHeader={true}
			viewStyle={{ backgroundColor: 'white', }}
			layoutBackground={{ backgroundColor: '#ff0000' }}
			customStatusbarColor={"#ff0000"}
			showLoading={showloading}
		// hideLeftComponentView={true}
		// hideMiddleComponentView={true}
		// hideRightComponentView={true}
		// rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
		>

			<View style={{ flex: 1 }}>
				<Image
					style={{
						height: '100%', width: Dimensions.get('window').width,
						opacity: 0.5
					}}

					source={AllSourcePath.LOCAL_IMAGES.background_one} />
			</View>

			<View style={{ paddingHorizontal: 30, position: 'absolute', width: '100%', height: Dimensions.get('window').height - 150, marginBottom: '50%' }}>
				<TouchableOpacity
					onPress={() => { props.navigation.goBack() }}
					style={{ paddingTop: 20 }}
				>
					<BackIcon />
				</TouchableOpacity>

				<ScrollView
					showsVerticalScrollIndicator={false}
					showsHorizontalScrollIndicator={false}
					style={{}}>

					<View style={{ flex: 1 }}>
						<View style={{ height: 30 }}></View>
						<View style={{}}>
							<Image
								style={{
									width: Dimensions.get('window').width / 3.1,
									alignSelf: 'center', marginVertical: 20,
									height: Dimensions.get('window').width / 3.1,
									borderRadius: Dimensions.get('window').width / 3.1
								}}
								source={AllSourcePath.LOCAL_IMAGES.login_prayLogo}
							/>
						</View>
						<View>
							<Text style={{ fontSize: 22, color: '#1D232A', textAlign: 'center' }}> OTP</Text>
							<Text style={{ fontSize: 14, color: '#1D232A', textAlign: 'center' }}> {'(One Time Password)'} </Text>
						</View>

						<View style={{ marginTop: 40 }}>
							<Text style={{ fontSize: 14, color: '#1D232A', textAlign: 'center' }}>
								Enter the code we've sent to your registered Email
							</Text>
						</View>


						<View style={{ marginTop: 40 }}>
							<OtpInput numInputs={4} onComplete={handleOtpComplete} />
						</View>

						<View style={{ marginTop: 40, justifyContent: 'center', flexDirection: 'row' }}>
							<Text style={{ fontSize: 12, color: '#1D232A', textAlign: 'center', paddingTop: 1 }}>
								Havenot receive OTP?
							</Text>
							<TouchableOpacity onPress={() => {
								func_resendOtp()
							}}>
								<Text style={{ fontSize: 14, color: '#7E6126', textAlign: 'center', paddingLeft: 5 }}>
									Resend
								</Text>
							</TouchableOpacity>
						</View>

					</View>

				</ScrollView>
			</View>


		</ScreenLayout>

	)
}

export default OtpScreen

