import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Theme from '../../Constants/Theme'

const Style = () => {

  return StyleSheet.create({
    topHeading:{marginTop:(Theme.windowHeight*0.11),textAlign:'center',fontSize:Theme.sizes.s16,color:Theme.colors.white,fontFamily:Theme.FontFamily.medium},
    sunHeadingSvgStyle:{alignSelf:'center',marginTop:36},

    borderStyleBase: {
      width: 30,
      height: 45
    },
  
    borderStyleHighLighted: {
      borderColor: "#03DAC6",
    },
  
    underlineStyleBase: {
      width: 30,
      height: 45,
      borderWidth: 0,
      borderBottomWidth: 1,
    },
  
    underlineStyleHighLighted: {
      borderColor: "#03DAC6",
    },

  })

}

export default Style
