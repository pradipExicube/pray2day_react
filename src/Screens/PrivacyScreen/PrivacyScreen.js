import { ActivityIndicator, Dimensions, RefreshControl, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import CustomHeader from '../../Components/Header/CustomHeader'

import RenderHTML from '../../Components/RenderHtml/RenderHtml'
import { useDispatch, useSelector } from 'react-redux'
import { Image } from 'react-native'

import HelperFunctions from '../../Constants/HelperFunctions'
import { api_postWithToken, getApi } from '../../Services/Service'
import Theme from '../../Constants/Theme'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import MenuIcon from '../../assets/icons/MenuIcon'
import { set_aboutusContent, set_privacyyContent } from '../../Store/Reducers/CommonReducer'
const { width, height } = Dimensions.get('screen');

const PrivacyScreen = (props) => {

	// const [Data, setData] = useState({})
	// const [Loder, setLoader] = useState(true);

	const [showloading, setShowloading] = useState(false);
	const dispatch = useDispatch();

	const { access_token, user_details, privacyyContent } = useSelector(state => state.commonData);


	useEffect(() => {
		if (!privacyyContent) {
			console.log("data not found")
			fetch_aboutus()
		} else {
			console.log("data found")
		}
	})

	const fetch_aboutus = () => {
		setShowloading(true)
		let params = {
			slug: 'privacy-policy'
		}
		// this.apiservice.api_fetchCMS('cms/contents',params)

		api_postWithToken('cms/contents', params, access_token)
			.then((response) => {
				setShowloading(false)
				if (response.data) {
					console.log("response.", response.data)
					if (response.data.cmscontent) {
						if (response.data.cmscontent.content) {
							dispatch(set_privacyyContent(response.data.cmscontent.content))
						}
					}

				}
			})
			.catch((error) => {
				setShowloading(false)
				HelperFunctions.showToastMsg(JSON.stringify(error));
			})

	}




	return (
		<ScreenLayout
			isScrollable={true}
			LeftComponent={''}
			// layoutBackground={{ backgroundColor: '#fff' }}
			IconColor={'#fff'}
			headerStyle={{ backgroundColor: '#eb9800' }}
			hideCustomHeader={false}
			viewStyle={{ backgroundColor: 'white', paddingHorizontal: 30 }}
			customStatusbarColor={"#ff0000"}
			showLoading={showloading}
			hideLeftComponentView={true}
			leftComponentOnPress={() => props.navigation.goBack()}

			// hideMiddleComponentView={true}
			hideRightComponentView={false}
			rightComponent={
				<TouchableOpacity
					style={{}}
					onPress={() => { props.navigation.navigate("MenuScreen") }}

				>
					<MenuIcon      
						menuIconColor={'#fff'}
						style={{ alignSelf: 'center', padding: 10, color: '#fff' }} />
				</TouchableOpacity>

			}
			// rightComponentOnPress={() => {props.navigation.navigate("MenuScreen")}}

			LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Privacy Policy</Text>}
		>


			{/* <Image
                resizeMode='cover'
                source={{ uri: Data?.image ? Data?.image : 'https://cdn.vectorstock.com/i/preview-1x/17/61/male-avatar-profile-picture-vector-10211761.jpg' }}
                style={styles.bannerImg}
            />
            <View style={{ padding: 20 }}>
                <Text style={{
                    fontFamily:Theme.FontFamily.bold,
                    color:Theme.colors.black,
                    fontSize:18,
                    marginBottom:10
                }}>Description</Text>
                {Data?.content && (
                    <RenderHTML html={Data?.content} />
                )}
            </View> */}
			<View>
				{privacyyContent && (
					<RenderHTML html={privacyyContent} />
				)}
			</View>

		</ScreenLayout>
	)
}

export default PrivacyScreen

const styles = StyleSheet.create({
	bannerImg: {
		height: 200,
		width: width - 40,
		borderRadius: 10,
		// marginHorizontal:20
		alignSelf: 'center',
		marginVertical: 15,
		borderWidth: 0.5,
		borderColor: '#BEBEBE'
	}
})