import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Theme from '../../Constants/Theme'

const Style = () => {
  
  return StyleSheet.create({
    articlesContainer:{
      width: Theme.windowWidth - 40,
      alignSelf: "center",
      flexDirection: "row",
      paddingVertical: 12,
      flex:1,
      borderBottomWidth:0.3,
      alignItems:'center'
    },
    subContainer:{
      flex: 7,
      flexDirection: "row",
      alignItems:'center'
    },
    itemContentTextStyle:{
      color: Theme.colors.textColor,
      fontSize: 12,
      fontFamily: Theme.FontFamily.medium,
      marginLeft: 10,
    },
    itemDateTextStyle:{
      color: Theme.colors.grey,
      fontSize: 12,
      fontFamily: Theme.FontFamily.medium,
      marginLeft: 10,
      marginTop:8
    }
  })

}

export default Style
