// import { , FlatList, ImageBackground, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import {  Dimensions, FlatList, Image, Pressable, Text, TouchableOpacity, View } from 'react-native'
const { width, height } = Dimensions.get('screen');

import Style from './Style';
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout';
import { useEffect, useState } from 'react';
import { api_postWithToken, getApi } from '../../Services/Service';
import HelperFunctions from '../../Constants/HelperFunctions';
import CustomScleton from '../../Components/CustomScleton/CustomScleton';
import { useSelector } from 'react-redux';
import MenuIcon from '../../assets/icons/MenuIcon';


const styles = Style();

const HomeScreen = (props) => {


  const [showSkleton, showShowSkleton] = useState(false);
  const [homeData, setHomeData] = useState({});
	const { access_token } = useSelector(state => state.commonData);



  useEffect(() => {
    showShowSkleton(true)
    fetchHomepageData()
  }, [])

  //== Fetch home page data
  const fetchHomepageData = () =>{
    // user/homepage/load
    api_postWithToken('user/homepage/load',{},access_token)
    .then((response) =>{
      showShowSkleton(false)
      if(response.status == "success"){
        console.log("Home page ==>", response.data)
        if(response?.data){
          setHomeData(response?.data);
          setTimeout(() => {
            checkSubscription()
          }, 1000);
        }
      }
    })
    .catch((error) => {
      showShowSkleton(false);
      HelperFunctions.showToastMsg(JSON.stringify(error))
    })
  }

  const checkSubscription =() =>{
    console.log("homeData : ", homeData)
    if(homeData?.active_subscription == true){
      // console.log("chbsdkc ====>", homeData?.banners?.top)
    }
    // else{
    //   props.navigation.replace('SubscriptionplanScreen');
    // }
  }




  const showFlatlistData =(data)=>{
    return (
      <View style={{margin:10}}>
        <Image
          style={{
              width: Dimensions.get('window').width-20,
              alignSelf: 'center',
              height: 200,
              borderRadius:5,
              // backgroundColor:'red'
          }}
          // source={AllSourcePath.LOCAL_IMAGES.forgetpass_logo}
          source={{ uri: data?.item?.image }}
          resizeMode='cover'
      />
      {/* <Text>csdkbcskd </Text> */}
      </View>
    )
  }



  const navigateToSubScreen=(item)=>{
    console.log("Pressable data =============================>", item)

    const dataToSend = { paramData: item };
    props.navigation.push('PrayerSubpageScreen',dataToSend)
  }

  const showlistData =(data)=>{
    return (
      <Pressable 
      onPress={()=>{
        
        navigateToSubScreen(data?.item)
        // props.navigation.navigate('PrayerSubpageScreen')
      }}
      >
      <View style={{
        paddingVertical:10,borderWidth:1,
        borderRadius:5,paddingHorizontal:10,
        flexDirection:'row',marginTop:15,
        borderColor:'#eb9800'
      }}>
      <View>
        <Image
            style={{
                width: 40,
                alignSelf: 'center',
                height: 40,
                borderRadius:5,
            }}
            // source={AllSourcePath.LOCAL_IMAGES.forgetpass_logo}
            source={{ uri: data?.item?.cover_image }}
            resizeMode='cover'
        />
      </View>
      <View style={{justifyContent:'center',paddingLeft:10,paddingRight:35}}>
        <Text style={{flexWrap:'wrap',fontWeight:600,letterSpacing:0.5}}>{data?.item?.name}</Text>
        </View>
    </View>
    </Pressable>
    )
  }

  

  return (

    <ScreenLayout
      isScrollable={false}
      LeftComponent={''}
      // layoutBackground={{ backgroundColor: '#fff' }}
      layoutBackground={'#eb9800' }
      headerStyle={{ backgroundColor: '#eb9800' }}
      hideCustomHeader={false}
      viewStyle={{ backgroundColor: 'white', 
        // paddingHorizontal: showSkleton ? showSkleton :30
      }}
      IconColor={'#fff'}
      customStatusbarColor={"#ff0000"}
      // showLoading={showloading}
      hideLeftComponentView={true}
      leftComponentOnPress={() => props.navigation.goBack()}
      LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Home Page</Text>}

      rightComponent={
        <TouchableOpacity
          style={{}}
          onPress={() => { props.navigation.navigate("MenuScreen") }}
        >
          <MenuIcon
            menuIconColor={'#fff'}
            style={{ alignSelf: 'center', padding: 10, color: '#fff' }} />
        </TouchableOpacity>

      }
      // refreshControl={
      //   <RefreshControl
      //     // refreshing={refreshing}
      //     onRefresh={() => {
      //       // setPage(1)
      //       // handleRefresh();
      //       // setrefreshing(true)
      //       // setTimeout(() => {
      //       //   setrefreshing(false)
      //       // }, 3000);
      //       // api_fetch_my_subscription('pulltorefresh')
      //     }}
      //     />
      //   }
    >
      {showSkleton ?  
          <CustomScleton /> 
        : 
        homeData?.active_subscription == true ? 
        <View style={{flex:1}}>
          <View>
              <FlatList
                horizontal
                // style={{width:100,backgroundColor:'red'}}
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                renderItem={showFlatlistData}
                data={homeData?.banners?.top}
                keyExtractor={(item, index) => index.toString()}
              />
          </View>
          
          <View style={{flex:1,marginHorizontal:10,marginBottom:10}}>
              <FlatList
                // pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                renderItem={showlistData}
                data={homeData?.prayers}
                keyExtractor={(item, index) => index.toString()}
              />
          </View>
        </View>
        :
        <View style={{justifyContent:'center',height:Dimensions.get('window').height/1.4}}>
          <Text style={{textAlign:'center',letterSpacing:0.5}}>No Active subscription found</Text>
          <Pressable
          onPress={()=>{props.navigation.replace("SubscriptionplanScreen")}}
          style={{marginTop:10}}>
            <Text style={{textAlign:'center',letterSpacing:0.5,fontSize:18,fontWeight:'500'}}>View plans</Text>
          </Pressable>
        </View>
      }
     
    </ScreenLayout>
  )

}

export default HomeScreen

