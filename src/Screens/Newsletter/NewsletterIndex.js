import React, { useState } from 'react';
import { Dimensions, FlatList, ImageBackground, SafeAreaView, StatusBar, StyleSheet, Text, View, Image, useAnimatedValue, TouchableOpacity, TextInput, ScrollView, Platform, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AllSourcePath from '../../Constants/PathConfig';
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout';
import Rightarrow from '../../assets/icons/Rightarrow';

import Theme from '../../Constants/Theme';
import HelperFunctions from '../../Constants/HelperFunctions';
import { postApi } from '../../Services/Service';

const NewsletterIndex = (props) => {
    const [name, setName] = useState("")
    const [lname, setLname] = useState("")
    const [email, setEmail] = useState("")
    const [Message, setMessage] = useState("")

    const [phoneNumber, setPhoneNumber] = useState('')
    const [Loder, setLoader] = useState(false);

    const Submit = () => {
       if (email == null || email == undefined || email.trim().length == 0) {
            HelperFunctions.showToastMsg("Please enter email")
        }
        else {
            setLoader(true)
            let data={
              
                "email":email,
            }
            postApi("newslettersubscribe",data,"").then(response=>{
              // console.log('response',response)
              if (response?.status) {
                
              //   props.navigation.replace("DrawerNavigation")
                setLoader(false)
                HelperFunctions.showToastMsg("Sent successfully")
                props.navigation.goBack()
                
              } else {
                HelperFunctions.showToastMsg("Please enter valid email")
                setLoader(false)
  
              }
            }).catch(error=>{
                HelperFunctions.showToastMsg("Please enter valid email")
              setLoader(false)
            }).finally(()=>{
              setLoader(false)
            })
      
          }
    }

    

    return (

        <ImageBackground style={{ flex: 1, }} source={AllSourcePath.LOCAL_IMAGES.SplashLayoutImage}>
            <StatusBar backgroundColor='transparent' showHideTransition={'slide'} barStyle={'dark-content'} hidden={false} translucent={true} networkActivityIndicatorVisible={true} />
            <SafeAreaView style={{
                marginTop: 20,
                flex: 1
            }}>
                <View style={{

                    flexDirection: "row",
                    marginBottom: "10%",
                    marginTop: 12
                }}>
                    <View style={{

                        padding: 12,
                        flex: 3.5
                    }}>
                        <Text style={{
                            alignSelf: "flex-end",
                            color: "#3A3A3C",
                            fontFamily: Theme.FontFamily.medium,
                            fontSize: 16
                        }}>NEWSLETTER</Text>
                    </View>
                    <View style={{

                        padding: 12,
                        flex: 2,
                    }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate("MenuScreen")}>
                            <Image source={require("../../assets/images/cross.png")}
                                style={{
                                    height: 24,
                                    width: 24,
                                    alignSelf: 'flex-end'
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView
                    contentContainerStyle={{
                        paddingTop: 25
                    }}
                >
                    <View style={{}}>
                        <Text style={{
                            color: "#3A3A3C",
                            fontSize: 20,
                            fontFamily: Theme.FontFamily.bold,
                            marginStart: "7%",
                            marginBottom: "10%",
                            // textAlign: "center",
                            // fontWeight:Theme.FontFamily.bold
                        }}>Join Our Newsletter!</Text>
                        <View style={{
                            // height: 445,
                            width: 342,
                            alignSelf: "center",
                            borderRadius: 12,
                            backgroundColor: "#FFFFFF",
                            marginBottom: "12%",
                            paddingVertical: 10,

                        }}>
                            <View style={{
                                marginHorizontal: 20,
                                // marginTop: 17,
                                // backgroundColor:"red"
                            }}>

                              
                                <View style={{
                                    width: "100%",
                                    borderBottomWidth: 1,
                                    borderColor: "#C1C0C0",
                                    paddingVertical: Platform.OS === 'ios' ? 12 : 3,
                                    marginBottom: Platform.OS === 'ios' ? 12 : 3
                                }}>
                                    <TextInput
                                        placeholder='EMAIL ADDRESS'
                                        
                                        placeholderTextColor={"#C1C0C0"}
                                        value={email}
                                        onChangeText={(text) => setEmail(text)}
                                        style={{
                                            fontSize: 16,
                                            fontFamily: Theme.FontFamily.medium,

                                        }}
                                    />
                                </View>
                               

                               
                                 
                                

                            </View>
                        </View>
                    </View>


                    <TouchableOpacity
                        onPress={() => Submit()}
                        disabled={Loder}
                        style={{
                            height: 60,
                            width: 342,
                            backgroundColor: "#3964AE",
                            borderRadius: 12,
                            alignSelf: "center",
                            justifyContent: "center",

                        }}

                    >
                        <Text style={{
                            textAlign: "center",
                            color: "#FFFFFF",
                            fontSize: 16,
                            fontFamily: Theme.FontFamily.medium,
                            letterSpacing: 4
                        }}>SUBSCRIBE</Text>
                        {Loder ?
                                    <ActivityIndicator style={{ marginHorizontal: 10 }} color={"#fff"} size={20} />
                                    :
                                    null
                                        }
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        </ImageBackground>
    )
}

export default NewsletterIndex;
