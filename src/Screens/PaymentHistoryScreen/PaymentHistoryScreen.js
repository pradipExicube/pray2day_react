import { Dimensions, FlatList, Image, ImageBackground, Linking, Pressable, RefreshControl, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import MenuIcon from '../../assets/icons/MenuIcon'
import { api_postWithToken, demoStripeAPi } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'
import { useDispatch, useSelector } from 'react-redux'
import { set_paymentHistory } from '../../Store/Reducers/CommonReducer'

const styles = Style()

const PaymentHistoryScreen = (props) => {
  const isFocused = useIsFocused()

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const [eyevalue, setEyevalue] = useState(true)
  const [showloading, setShowloading] = useState(false)
  const [showTick, setShowTick] = useState(false)


  const dispatch = useDispatch();
  const [refreshing, setrefreshing] = useState(false)

  const { access_token, user_details, store_paymentHistory } = useSelector(state => state.commonData);


  useEffect(() => {

    if(store_paymentHistory.length  == 0){
      api_fetchTransactionHistory('null'); 
    }
  }, [])


  const api_fetchTransactionHistory = (value) =>{
    if(value == 'pulltorefresh'){
      setrefreshing(true)
    }else{
      setShowloading(true)
    }
    api_postWithToken('user/subscription/history', {}, access_token)
    .then(response => {
      if(value == 'pulltorefresh'){
        setrefreshing(false)
      }else{
        setShowloading(false)
      }

      if (response.status == 'success') {
        const sub_data = response.data;
        if (sub_data?.reports) {
          dispatch(set_paymentHistory(sub_data?.reports));
        } 
      } else {
        HelperFunctions.showToastMsg(response.message)
      }

    }).catch(error => {
      if(value == 'pulltorefresh'){
        setrefreshing(false)
      }else{
        setShowloading(false)
      }
    })
  }

  // == Fetch invoice details
  const fetch_invoiceDetails = (details) =>{
    setShowloading(true);
    let params={
      "id": details?.id
    }
    api_postWithToken('user/subscription/transaction/details',params,access_token)
    .then(response => {
      setShowloading(false);

      if (response.status == 'success') {
        const invoice_data = response.data;
        if (invoice_data?.receipt_url) {
          // console.log("Invoice data ======>",invoice_data?.receipt_url)
          Linking.openURL(invoice_data?.receipt_url)
        } 
      } else {
        HelperFunctions.showToastMsg(response.message)
      }

    }).catch(error => {
        setShowloading(false)
    })
    

  }



  const showFlatlistData = (data) => {
    console.log("showFlatlistData ====>", data)
    return (
      <View
        style={{
          borderWidth: 0.4, paddingVertical: 15, paddingHorizontal: 15,
          borderRadius: 5, borderColor: '#E0E8E5', backgroundColor: '#fff', marginTop: 15,
          shadowColor: '#000',
          shadowOffset: { width: 0, height: 0.3 },
          shadowOpacity: 0.2,
          shadowRadius: 0.5,  

        }}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View>
            <View style={{paddingBottom:10}}><Text style={{fontSize:16,color:'#404040',letterSpacing:0.5,fontWeight:500}}>Prime membership</Text></View>
            <View style={{paddingBottom:10}}><Text style={{fontSize:14,color:'#1d1d1d',letterSpacing:0.5}}>Amount - $5.99</Text></View>
            <View><Text style={{fontSize:14,color:'#05a371',letterSpacing:0.5}}>Monthly</Text></View>
          </View>
          <View style={{ justifyContent: 'space-between' }}>
            <View><Text style={{ fontSize: 12, letterSpacing: 0.5, color: '#99AEA6' }}> 18 Feb 24 - 01:49 PM </Text></View>
            <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
            <Pressable onPress={()=>{fetch_invoiceDetails(data?.item)}}>
                <Image
                  style={{ height: 25, width: 25 }}
                  source={AllSourcePath.LOCAL_IMAGES.invoice_img}
                />
              </Pressable>
            </View>
          </View>
        </View>


      </View>


    )
  }

  return (
    <ScreenLayout
      isScrollable={true}
      LeftComponent={''}
      // layoutBackground={{ backgroundColor: '#fff' }}
      layoutBackground={'#eb9800' }
      IconColor={'#fff'}
      headerStyle={{ backgroundColor: '#eb9800' }}
      hideCustomHeader={false}
      viewStyle={{ backgroundColor: 'white', paddingHorizontal: 20 }}
      customStatusbarColor={"#ff0000"}
      showLoading={showloading}
      hideLeftComponentView={true}
      leftComponentOnPress={() => props.navigation.goBack()}

      // hideMiddleComponentView={true}
      hideRightComponentView={false}
      rightComponent={
        <TouchableOpacity
          style={{}}
          onPress={() => { props.navigation.navigate("MenuScreen") }}
        >
          <MenuIcon
            menuIconColor={'#fff'}
            style={{ alignSelf: 'center', padding: 10, color: '#fff' }} />
        </TouchableOpacity>

      }
      // rightComponentOnPress={() => {props.navigation.navigate("MenuScreen")}}

      LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>payment History</Text>}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={() => {
            api_fetchTransactionHistory('pulltorefresh')
          }}
        />
      }
    >


      {/* <View style={{paddingTop:20}}> */}
        <FlatList
          renderItem={showFlatlistData}
          data={store_paymentHistory}
          keyExtractor={(item, index) => index.toString()}
        />
      {/* </View> */}

    </ScreenLayout>

  )
}

export default PaymentHistoryScreen

