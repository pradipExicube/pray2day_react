import { Dimensions, Image, ImageBackground, Pressable, SafeAreaView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'

const styles = Style()

const IntroScreen = (props) => {
    const isFocused = useIsFocused()

    useEffect(() => {
        setTimeout(() => {
            // props.navigation.replace("LoginScreen")
            // props.navigation.replace("SubscriptionplanScreen")
            
        }, 2000);
    }, [])

    return (
        <ScreenLayout
            isScrollable={false}
            LeftComponent={false}
            hideCustomHeader={true}
            viewStyle={{ backgroundColor: 'white' }}
            layoutBackground={{ backgroundColor: '#ff0000' }}
            customStatusbarColor={"#ff0000"}
        // hideLeftComponentView={true}
        // hideMiddleComponentView={true}
        // hideRightComponentView={true}
        // rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
        >
            <View style={{ flex: 1}}>
                <View style={{ flex:1 }}>
                
                <Image 
                style={{ 
                // alignSelf: 'center', 
                // marginVertical: 20, 
                // objectFit:'fit',
                height: '100%',width:Dimensions.get('window').width,
                opacity:0.5
                 }} 
                
                source={AllSourcePath.LOCAL_IMAGES.background_one} />
                

                <View style={{position:'absolute',bottom:0,flexDirection:'column',justifyContent:'center',width:'100%'}}>
                    <View>
                        <Image style={{ width: Dimensions.get('window').width / 2.1, alignSelf: 'center', marginVertical: 20, height: Dimensions.get('window').width / 2.1, borderRadius: Dimensions.get('window').width / 2.1 }} source={AllSourcePath.LOCAL_IMAGES.prayerLogo} />
                    </View>
                </View>

                </View>
                {/* <View style={{flexDirection:'row',justifyContent:'center'}}> */}

                 <View style={{flex:1,marginTop:'1%'}}>
                 <View>
                    <Text style={{ fontSize: 24, textAlign: 'center' }}> Lets Pray </Text>
                </View>
                <View style={{paddingHorizontal:30,paddingTop:20}}>
                </View>

                <View style={{paddingHorizontal:30,paddingTop:40}}>
                    <CustomButton 
                        buttonText={"Get Started"} 
                        customIconName="arrow-up" 
                        customIconStyle={{transform: [{rotateZ: '45deg'}]}}
                        onPress={()=>{
                            props.navigation.replace("LoginScreen")
                        }}
                    />
                </View>

                 </View>

                {/* </View> */}
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingVertical: 20 }}>
                <Text> Do you have a account? </Text>
                <Pressable onPress={()=>{props.navigation.replace("LoginScreen")}}>
                    <Text style={{ color: '#eb9800' }}>Login</Text>
                </Pressable>
            </View>

        </ScreenLayout>

    )
}

export default IntroScreen

