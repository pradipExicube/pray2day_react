import { Dimensions, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import { api_postWithToken, api_postWithoutToken } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'
import { deleteData, getData, setData } from '../../Services/LocalStorage'
import { useDispatch, useSelector } from 'react-redux'
import { set_subscriptionDetails, store_setAccessToken, store_setUserdetails } from '../../Store/Reducers/CommonReducer'

const styles = Style()

const LoginScreen = (props) => {
    const isFocused = useIsFocused()
    const dispatch = useDispatch();

    // const [email, setEmail] = useState("user002@yopmail.com")
    // const [email, setEmail] = useState("user003@yopmail.com")
    // const [password, setPassword] = useState("12345678")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const [eyevalue, setEyevalue] = useState(true)
    const [showloading, setShowloading] = useState(false)

    const { access_token ,user_details} = useSelector(state => state.commonData);

    useEffect(() => {
        // setTimeout(() => {
        //     props.navigation.replace("HomeScreen")
        // }, 2000);
        // setTimeout(() => {
        //     setShowloading(false);
        // }, 5000);

    }, [])


    // = Email validation ====
    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
          console.log("Email is Not Correct");
          setEmail(text)
          return false;
        }
        else {
          setEmail(text)
          console.log("Email is Correct");
          return true;
        }
      }

    // = Login function ====
    const startLogin = () => {
        if(email && password) {
            const isemailValidate = validate(email);
            if(isemailValidate == true){
                setShowloading(true)
                let paramData={
                    email:email,
                    password:password,
                    // fcm_token:this.global.deviceToken ? this.global.deviceToken :""
                }
                api_postWithoutToken('auth/login',paramData).then(response=>{
                    if(response.status=='success'){
                        // console.log("success : ", JSON.stringify(response.data))
                        const successdata = response.data; 
                        setData('access_token',successdata?.token?.access_token)
                        setData('user_details',JSON.stringify(successdata.user))
                        dispatch(store_setAccessToken(successdata?.token?.access_token))
                        dispatch(store_setUserdetails(successdata.user))

                        setTimeout(() => {
                            api_fetch_my_subscription(successdata?.token?.access_token)
                        }, 1000);

                    }else{
                        setShowloading(false)
                        HelperFunctions.showToastMsg(response.message)
                    }
                
                }).catch(error=>{
                    setShowloading(false)
                    // changeloadingState(false)
                    HelperFunctions.showToastMsg(error.message)
                })
            }

        }else{
            if(!email){
                HelperFunctions.showToastMsg('Email can not be empty');
                
            }else if(!password){
                HelperFunctions.showToastMsg('Password can not be empty');

            }
        }
    }

    // Fetch subscription status
    const api_fetch_my_subscription = (userToken) => {
        console.log("userToken : ", userToken)
        api_postWithToken('user/subscription/active-plan', {}, userToken).then(response => {
            setShowloading(false)
            if (response.status == 'success') {
                // Object.keys(globalData).length === 0
                const sub_data = response.data;
                if (sub_data?.subscription) {
                    dispatch(set_subscriptionDetails(sub_data?.subscription));
                    props.navigation.replace('HomeScreen')
                    // props.navigation.replace('SubscriptionplanScreen')
                } else {
                    props.navigation.replace('SubscriptionplanScreen')
                }
            } else {
                HelperFunctions.showToastMsg(response?.message)
            }
    
        }).catch(error => {
            setShowloading(false);
            HelperFunctions.showToastMsg(JSON.stringify(error))
        })
      }



    return (
        <ScreenLayout
            isScrollable={false}
            LeftComponent={false}
            hideCustomHeader={true}
            viewStyle={{ backgroundColor: 'white', }}
            // layoutBackground={{ backgroundColor: '#ff0000' }}
            
            customStatusbarColor={"#ff0000"}
            showLoading={showloading}
        // hideLeftComponentView={true}
        // hideMiddleComponentView={true}
        // hideRightComponentView={true}
        // rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
        >
            
            <View style={{ flex: 1 }}>
                <Image
                    style={{
                        // alignSelf: 'center', 
                        // marginVertical: 20, 
                        // objectFit: 'fit',
                        height: '100%', width: Dimensions.get('window').width,
                        opacity: 0.5
                    }}

                    source={AllSourcePath.LOCAL_IMAGES.background_one} />
            </View>
            <ScrollView style={{ paddingHorizontal: 30, position: 'absolute', height: '100%', width: '100%' }}>

                <View style={{ flex: 1 }}>
                    <View style={{ height: 50 }}></View>
                    <View style={{}}>
                        <Image
                            style={{
                                width: Dimensions.get('window').width / 3.1,
                                alignSelf: 'center', marginVertical: 20,
                                height: Dimensions.get('window').width / 3.1,
                                borderRadius: Dimensions.get('window').width / 3.1
                            }}
                            source={AllSourcePath.LOCAL_IMAGES.login_prayLogo}
                        />
                    </View>
                    <View>
                        <Text style={{ fontSize: 24, color: '#1D232A' }}> Sign in to</Text>
                        <Text style={{ fontSize: 24, color: '#1D232A' }}> your account </Text>
                    </View>
                    <View style={{ paddingTop: 50 }}>
                        <EditTextComponent
                            style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                            placeholder={"Email*"}
                            // iconName={eyevalue ? 'eye-off-outline' : 'eye-outline'}
                            // iconPress={() => { setEyevalue(!eyevalue) }}
                            secureTextEntry={false}
                            value={email}
                            onChangeText={(textValue) => { validate(textValue) }}
                        />
                    </View>

                    <View style={{ paddingTop: 20 }}>
                        <EditTextComponent
                            style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                            placeholder={"Password*"}
                            iconName={eyevalue ? 'eye-off-outline' : 'eye-outline'}
                            iconPress={() => { setEyevalue(!eyevalue) }}
                            secureTextEntry={eyevalue ? true : false}
                            value={password}
                            onChangeText={(passValue) => { setPassword(passValue) }}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingVertical: 15 }}>
                        <Pressable onPress={() => {
                            props.navigation.navigate("ForgotpassScreen")
                        }}>
                            <Text style={{ color: '#1D232A' }}>Forgot Password</Text>
                        </Pressable>
                    </View>

                    <View style={{ paddingTop: 40 }}>
                        <CustomButton
                            buttonText={"Sign in"}
                            customIconName="arrow-up"
                            customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
                            onPress={() => { 
                                // props.navigation.replace("SubscriptionplanScreen")
                                startLogin()
                            }}
                        />
                    </View>
                </View>

            </ScrollView>

            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingVertical: 20 }}>
                <Text> New account? </Text>
                <Pressable onPress={() => {
                    props.navigation.replace("SignupScreen")
                }}>
                    <Text style={{ color: '#eb9800' }}>Registration</Text>
                </Pressable>
            </View>

        </ScreenLayout>

    )
}

export default LoginScreen

