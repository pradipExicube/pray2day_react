import { Dimensions, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import HelperFunctions from '../../Constants/HelperFunctions'
import { api_postWithoutToken } from '../../Services/Service'

const styles = Style()

const ResetpassScreen = (props) => {
    const isFocused = useIsFocused()

    const [newPassword, setNewPassword] = useState("")
    const [showNewPassword, setShowNewPassword] = useState(true)

    const [confPassword, setConfPassword] = useState("")
    const [showConfPassword, setShowConfPassword] = useState(true)

    const [otp, setOtp] = useState("")


    const [eyevalue, setEyevalue] = useState(true)
    const [showloading, setShowloading] = useState(false)    
    const { otp_token,otp_email,passwordReset_type } = props?.route?.params;


    useEffect(() => {
        // setTimeout(() => {
        //     props.navigation.replace("HomeScreen")
        // }, 2000);
        // setTimeout(() => {
        //     setShowloading(false);
        // }, 5000);
    }, [])


    // useEffect(() => {
    //     if(param1){
    //         console.log("OTP token : ", param1)
    //         setParamToken(param1)
    //     }
    // }, [otp_token])

    const func_reset = () =>{
        console.log("OTP token : ", otp_token)
        console.log("OTP email : ", otp_email)
        if(!newPassword){
            HelperFunctions.showToastMsg("New Password field can not be blnk")
        }else if(!confPassword){
            HelperFunctions.showToastMsg("Confirm Password field can not be blnk")
        }else if(!otp){
            HelperFunctions.showToastMsg("OTP field can not be blnk")
        }else{
            call_resetPassword()
        }
    }
    const call_resetPassword = () =>{
        if(otp_token && otp_email && passwordReset_type){
            setShowloading(true);
            let params={
                type:passwordReset_type,
                verification_token:otp_token,
                otp:otp,
                new_password:newPassword,
                new_password_confirmation:confPassword
            }
            api_postWithoutToken('otp/verify',params)
            .then((response)=>{
                setShowloading(false);
                if(response.status == 'success'){
                    HelperFunctions.showToastMsg(response.message);
                    props.navigation.replace('LoginScreen');
                }else{
                    HelperFunctions.showToastMsg(response.message);
                }
            })
            .catch((error)=>{
                setShowloading(false);
                HelperFunctions.showToastMsg(JSON.stringify(error));
            })



        }else{
            HelperFunctions.showToastMsg("Previous screen params missing, Kindly try again")
        }
    }




    return (
        <ScreenLayout
            isScrollable={false}
            LeftComponent={''}
            layoutBackground={{ backgroundColor:'#fff'}}
            headerStyle={{backgroundColor:'#fff'}}
            hideCustomHeader={false}
            viewStyle={{ backgroundColor: 'white', }}
            customStatusbarColor={"#ff0000"}
            showLoading={showloading}
            hideLeftComponentView={false}
            leftComponentOnPress={()=>props.navigation.goBack()}
            // hideMiddleComponentView={true}
            hideRightComponentView={true}
            // rightComponentOnPress={() => props.navigation.navigate("MenuScreen")}
        >
            
            <View style={{ flex: 1 }}>
                {/* <Image
                    style={{
                        // alignSelf: 'center', 
                        // marginVertical: 20, 
                        objectFit: 'fit',
                        height: '100%', width: Dimensions.get('window').width,
                        opacity: 0.5
                    }}

                    source={AllSourcePath.LOCAL_IMAGES.background_one} /> */}
            </View>
            <ScrollView style={{ paddingHorizontal: 30, position: 'absolute', height: '100%', width: '100%' }}>

                <View style={{ flex: 1 }}>
                    <View style={{ height: 50 }}></View>
                    <View style={{}}>
                        <Image
                            style={{
                                width: Dimensions.get('window').width / 3.1,
                                alignSelf: 'center', marginVertical: 20,
                                height: Dimensions.get('window').width / 3.1,
                                borderRadius: Dimensions.get('window').width / 3.1
                            }}
                            source={AllSourcePath.LOCAL_IMAGES.login_prayLogo}
                        />
                    </View>
                    <View>
                        <Text style={{ fontSize: 24, color: '#1D232A',textAlign:'center' }}> Reset Password</Text>
                        <Text style={{  color: '#1D232A',textAlign:'center',paddingTop:20 }}> Enter the code we've sent via Email to your registered email </Text>
                    </View>
                    <View style={{ paddingTop: 50 }}>
                        <EditTextComponent
                            style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                            placeholder={"New Password*"}
                            iconName={showNewPassword ? 'eye-off-outline' : 'eye-outline'}
                            iconPress={() => { setShowNewPassword(!showNewPassword) }}
                            secureTextEntry={showNewPassword ? true : false}
                            value={newPassword}
                            onChangeText={(newPassValue) => { setNewPassword(newPassValue) }}
                        />
                    </View>

                    <View style={{ paddingTop: 20 }}>
                        <EditTextComponent
                            style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                            placeholder={"Confirm Password*"}
                            iconName={showConfPassword ? 'eye-off-outline' : 'eye-outline'}
                            iconPress={() => { setShowConfPassword(!showConfPassword) }}
                            secureTextEntry={showConfPassword ? true : false}
                            value={confPassword}
                            onChangeText={(confPassValue) => { setConfPassword(confPassValue) }}
                        />
                    </View>

                    <View style={{ paddingTop: 20 }}>
                        <EditTextComponent
                            style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8' }}
                            placeholder={"Enter OTP*"}
                            value={otp}
                            onChangeText={(otpValue) => { setOtp(otpValue) }}
                            maxLength={4}
                            keyboardType={'phone-pad'}
                            
                        />
                    </View>

                    {/* <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingVertical: 15 }}>
                        <Pressable onPress={() => {

                        }}>
                            <Text style={{ color: '#1D232A' }}>Forgot Password</Text>
                        </Pressable>
                    </View> */}

                    <View style={{ paddingTop: 40 }}>
                        <CustomButton
                            buttonText={"Reset Now"}
                            customIconName="arrow-up"
                            customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
                            onPress={() => { func_reset()}}
                        />
                    </View>
                </View>

            </ScrollView>

            {/* <View style={{ flexDirection: 'row', justifyContent: 'center', paddingVertical: 20 }}>
                <Text> New account? </Text>
                <Pressable onPress={() => {
                    props.navigation.replace("SignupScreen")
                }}>
                    <Text style={{ color: '#eb9800' }}>Registration</Text>
                </Pressable>
            </View> */}

        </ScreenLayout>

    )
}

export default ResetpassScreen

