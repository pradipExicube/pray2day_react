// import { , FlatList, ImageBackground, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { ActivityIndicator, Dimensions, FlatList, Image, ImageBackground, Linking, Modal, Pressable, RefreshControl, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
const { width, height } = Dimensions.get('screen');


import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout';
import Theme from '../../Constants/Theme';
import IonIcon from 'react-native-vector-icons/Ionicons'
import LinearGradient from 'react-native-linear-gradient';
import AllSourcePath from '../../Constants/PathConfig';
import Righticon from '../../assets/icons/Righticon';
import { useEffect, useState } from 'react';
import { getApi } from '../../Services/Service';
import HelperFunctions from '../../Constants/HelperFunctions';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import NodataFound from '../../Components/NoData/NodataFound';






const ProviderScreen = (props) => {
  const { width, height } = Dimensions.get('screen');
  const list = [
    { id: 1, img: require("../../assets/images/p.png"), arrow: require("../../assets/images/down.png") },
    { id: 2, img: require("../../assets/images/i.png") },
    { id: 3, img: require("../../assets/images/k.png") },
    { id: 4, img: require("../../assets/images/g.png") },
    { id: 5, img: require("../../assets/images/uaic.png") },
    { id: 6, img: require("../../assets/images/f.png") },

  ]
  const [Data,setData] = useState([])
  const [refreshing, setrefreshing] = useState(false);


  const [Loder, setLoader] = useState(true);
  const getAllProvider = async() => {
    getApi("insurance-providers","","").then(response=>{
     // console.log('getAllProvider',response)
     if (response?.status) {
       setLoader(false)
       setData(response?.data)
      //  setData([])
       
      //  setTitle(response?.data?.home_title)
     } else {
       HelperFunctions.showToastMsg(response?.error)
       setLoader(false)
       setData([])
     }
   }).catch(error=>{
       HelperFunctions.showToastMsg(error?.message)
       setLoader(false)
   }).finally(()=>{
     setLoader(false)
   })
 }

 const handleRefresh = () => {
  setrefreshing(true);
  
  getAllProvider()

  setTimeout(() => {
    setrefreshing(false);
  }, 1000);

  // console.log(UserData)
};
 useEffect(()=>{
  getAllProvider()
 },[])
 if (Loder) {
  return (
    <View style={{flex:1,height:'100%',width:'100%',alignItems:'center'}}>
        <StatusBar
			backgroundColor={'#E6E6E6'}
			animated={true}
			// barStyle={props.Margin ? 'dark-content' : 'light-content'}
			translucent={true}
			/>
    <FlatList
    showsVerticalScrollIndicator={false}
    style={{ alignSelf: 'center', width: '100%', marginBottom: '3%',marginTop: "30%"}}
    renderItem={() => {
      return (
        <SkeletonPlaceholder borderRadius={4}>
          <SkeletonPlaceholder.Item
            justifyContent="center"
            height={80}
            width={'100%'}
            marginVertical={5}
            flexDirection="row"
            marginTop={'0%'}
            borderRadius={5}
            paddingHorizontal={0}
          >
           
             
              
                <SkeletonPlaceholder.Item
                  width={"90%"} height={'90%'}
                />
              </SkeletonPlaceholder.Item>
            
         
        </SkeletonPlaceholder>
      )
    }}
    data={[1, 1,1,1,1,1,1,1,1]}
  />
  </View>
  )
}
  const renderItem = ({ item }) => (
    <Pressable 
    onPress={()=>{
      Linking.openURL(item?.link)
    }}
    style={{
      height: 80,
      width: "90%",
      backgroundColor: "white",
      marginHorizontal: 20,
      marginTop: 10,
      borderRadius: 10,
      backgroundColor: 'white',
      borderRadius: 8,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: 10,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.2,
          shadowRadius: 4,
        },
        android: {
          elevation: 4,
        },
      }),
    }}>


      <Image source={{uri:item?.logo}}
      resizeMode='contain'
        style={{
          height: 65,
          width: '65%',
          borderRadius:5
        }}
      />
      <Righticon />
    </Pressable>

  )

  return (
    <ScreenLayout
      leftComponentOnPress={() => props.navigation.goBack()}
      rightComponent
      LeftComponentextrat
      middleText={"INSURANCE PROVIDERS"}
      middleTextStyle={{color:"#3A3A3C", fontFamily: Theme.FontFamily.medium,
      fontSize: 16}}
    >
      <LinearGradient colors={['#E6E6E6', '#E6E6E6', '#F4F4F4', "#FFFFFF1A"]} style={{ flex: 1 }} >
        {Data.length == 0 ? <NodataFound/> :
        <FlatList
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => {
                // setPage(1)
                handleRefresh();
              }}
            />
          }
          contentContainerStyle={{ paddingBottom: 30 }}
          data={Data}
          renderItem={renderItem}
        />
}
      </LinearGradient>
    </ScreenLayout>
  )

}

export default ProviderScreen