import { StyleSheet, Text, View,Dimensions } from 'react-native'
import React from 'react'
import Theme from '../../Constants/Theme';
const { width, height } = Dimensions.get('screen');
const Style = () => {
  return StyleSheet.create({
    mainbox:{
      height: 50,
      width: width - 40,
      backgroundColor: "#FFFFFF",
      alignSelf: "center",
      borderRadius: 5,
      marginTop: 10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.1,
      shadowRadius: 2,
      justifyContent: "center",
      elevation: 5, 
    },
    insidecontent:{
      flexDirection: "row",
      justifyContent: "space-between",
      marginHorizontal: 10
    },
    titletext:{
      alignSelf: "center",
      color: "#0C0000",
      fontSize: 16,
      fontFamily: Theme.FontFamily.medium,
    },
    topsection:{
      
        marginHorizontal: 20,
        marginBottom: 30,
        marginTop:10
     
    },
    nowbutton:{
      height: 48,
      width: width - 40,
      backgroundColor: "#E5782A",
      borderRadius: 7,
      alignSelf: "center",
      justifyContent: "center",
      marginBottom: 20
    },
    accesstext:{
      color: "#0C0000",
      fontSize: 12,
      fontFamily: Theme.FontFamily.medium,
      marginHorizontal: 20,

    },
    othertext:{
      color: "#0C0000",
      fontSize: 12,
      fontFamily: Theme.FontFamily.medium,
      marginHorizontal: 20,
      marginBottom: 20
    },
    contactsection:
    {
       
      width: width - 40,
      backgroundColor: "#FFFFFF",
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.1,
      shadowRadius: 2,
      justifyContent: "center",
      elevation: 5,
      alignSelf: "center",
      borderRadius: 8,
      marginBottom:20

    },
    indexsidecontact:{
      paddingVertical: 15,
      borderBottomWidth: 1,
      borderColor: "#0000000D",
     
    },
    innersection: {
      flexDirection: "row",
      justifyContent: "space-between",
      width: "90%",
      alignSelf:"center"
    },
    contacttext: {
      color: "#0C0000",
      fontFamily: Theme.FontFamily.medium,
      fontSize: 16
    },
    nowtext:{
      color: "#FFFFFF",
      fontSize: 14,
      fontFamily: Theme.FontFamily.medium,
      textAlign: "center"
    }
    
  })
}

export default Style
