import { Dimensions, FlatList, Image, ImageBackground, Pressable, RefreshControl, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import MenuIcon from '../../assets/icons/MenuIcon'
import { api_postWithToken, demoStripeAPi } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'

import { PlatformPay, initStripe, useStripe } from '@stripe/stripe-react-native';
import { useSelector } from 'react-redux'
import { set_subscriptionDetails } from '../../Store/Reducers/CommonReducer'

const styles = Style()

const MySubscriptionScreen = (props) => {
  const isFocused = useIsFocused()

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const [eyevalue, setEyevalue] = useState(true)
  const [showloading, setShowloading] = useState(false)
  const [showTick, setShowTick] = useState(false)

  const [refreshing, setrefreshing] = useState(false)
  
  const { access_token, user_details, subscriptionDetails } = useSelector(state => state.commonData);

  

  const { initPaymentSheet, presentPaymentSheet,sub } = useStripe();


  useEffect(()=>{
    console.log("My subscription : ", subscriptionDetails)
  })


  const api_fetch_my_subscription = (value) => {
    if(value == 'pulltorefresh'){
      setrefreshing(true)
    }else{
      setShowloading(true)
    }
    api_postWithToken('user/subscription/active-plan', {}, access_token).then(response => {
      if(value == 'pulltorefresh'){
        setrefreshing(false)
      }else{
        setShowloading(false)
      }

      if (response.status == 'success') {
        // Object.keys(globalData).length === 0
        const sub_data = response.data;
        if (sub_data?.subscription) {
          console.log("subscription ", subscriptionDetails);
          dispatch(set_subscriptionDetails(sub_data?.subscription));
        } 
      } else {
        // HelperFunctions.showToastMsg(response.message)
      }

    }).catch(error => {
      if(value == 'pulltorefresh'){
        setrefreshing(false)
      }else{
        setShowloading(false)
      }
    })
  }







  return (
    <ScreenLayout
      isScrollable={true}
      LeftComponent={''}
      // layoutBackground={{ backgroundColor: '#fff' }}
      layoutBackground={'#eb9800' }
      IconColor={'#fff'}
      headerStyle={{ backgroundColor: '#eb9800' }}
      hideCustomHeader={false}
      viewStyle={{ backgroundColor: 'white', paddingHorizontal: 30 }}
      customStatusbarColor={"#ff0000"}
      showLoading={showloading}
      hideLeftComponentView={true}
      leftComponentOnPress={() => props.navigation.goBack()}
    //   RefreshControl={
    //   <RefreshControl refreshing={refreshing} onRefresh={()=>{setrefreshing(true)}} />
    // }
    refreshControl={
      <RefreshControl
        refreshing={refreshing}
        onRefresh={() => {
          // setPage(1)
          // handleRefresh();
          // setrefreshing(true)
          // setTimeout(() => {
          //   setrefreshing(false)
          // }, 3000);
          api_fetch_my_subscription('pulltorefresh')
        }}
      />
    }

      // hideMiddleComponentView={true}
      hideRightComponentView={false}
      rightComponent={
        <TouchableOpacity
          style={{}}
          onPress={() => { props.navigation.navigate("MenuScreen") }}
        >
          <MenuIcon


            menuIconColor={'#fff'}
            style={{ alignSelf: 'center', padding: 10, color: '#fff' }} />
        </TouchableOpacity>

      }
      // rightComponentOnPress={() => {props.navigation.navigate("MenuScreen")}}

      LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Subscription Plans</Text>}
    >

      {subscriptionDetails?.plan_name ? 

      <View style={{ borderWidth: 0.2, paddingVertical: 10, paddingHorizontal: 10, borderRadius: 5,borderColor:'#E0E8E5',backgroundColor:'#fff',marginTop:30 }}>
        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ justifyContent: 'center' }}>
                <Image
                  style={{ height: 15, width: 15, borderRadius: 15 }}
                  source={AllSourcePath.LOCAL_IMAGES.corrent_img}
                />
              </View>
              <View style={{ justifyContent: 'center', paddingLeft: 5 }}>
                <Text style={{ fontSize: 16, letterSpacing: 0.5 }}>{subscriptionDetails?.plan_name}</Text>
              </View>
            </View>
            <View style={{ justifyContent: 'center', paddingLeft: 20, paddingTop: 3 }}>
              <Text style={{ fontSize: 14, letterSpacing: 0.5, color: '#99AEA6' }}>Duration - {subscriptionDetails?.duration} Days</Text>
            </View>

          </View>
          <View 
          // style={{borderWidth:0.4,paddingVertical:5,paddingHorizontal:5,borderRadius:5,backgroundColor:'red',justifyContent:'center'}}
          >
            <Text style={{ fontSize: 18, letterSpacing: 0.5,fontWeight:500, }}>${subscriptionDetails?.amount}</Text>
          </View>

        </View>
        <View style={{paddingTop:20}}>
          <Text style={{ fontSize: 14, letterSpacing: 0.5, color: '#fc5252' }}> Expire on {subscriptionDetails?.expiry_at} </Text>
        </View>

      </View>
      :
        <View style={{justifyContent:'center',height:Dimensions.get('window').height/1.4}}>
          <Text style={{textAlign:'center',letterSpacing:0.5}}>No Active subscription found</Text>
          <Pressable 
          onPress={()=>{props.navigation.replace("SubscriptionplanScreen")}}
          style={{marginTop:10}}>
            <Text style={{textAlign:'center',letterSpacing:0.5,fontSize:18,fontWeight:'500'}}>View plans</Text>
          </Pressable>
        </View>
      }



    </ScreenLayout>

  )
}

export default MySubscriptionScreen

