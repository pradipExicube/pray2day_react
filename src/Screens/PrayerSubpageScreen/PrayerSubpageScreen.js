import { Dimensions, FlatList, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import { useDispatch, useSelector } from 'react-redux'
import { api_postWithToken } from '../../Services/Service'
import { set_store_chaptarDetails } from '../../Store/Reducers/CommonReducer'
import CustomScleton from '../../Components/CustomScleton/CustomScleton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import CustomButton from '../../Components/CustomButton/CustomButton'
import CustomButtonLeftIcon from '../../Components/CustomButtonLeftIcon/CustomButtonLeftIcon'
import HelperFunctions from '../../Constants/HelperFunctions'
import TrackPlayer from 'react-native-track-player';
import { onRegisterPlayback,addTrack, setUpPlayer, pauseTrack, playTrack, stopTrack, resetTrack } from '../../Services/TrackPlayerService'

const styles = Style()

const PrayerSubpageScreen = (props) => {
	const isFocused = useIsFocused()
	const dispatch = useDispatch()

	const [showloading, setShowloading] = useState(false)
	const [showSkleton, showShowSkleton] = useState(false);
	const [prevScreenParam, setprevScreenParam] = useState(null)

	const [isPlayTrack, setisPlayTrack] = useState(false)

	const [prayerNo, setprayerNo] = useState('')


	const { access_token, store_chaptarDetails } = useSelector(state => state.commonData);

	const { paramData } = props?.route?.params;

	useEffect(() => {
			dispatch(set_store_chaptarDetails(null))
			if(paramData){
					console.log("<======== sub page ====> : ", paramData)
					setprevScreenParam(paramData);
					// setParamToken(param1)
					showShowSkleton(true)
					fetch_PrayerChapter(paramData)
			}
	}, [paramData])

	useEffect(() => {
		// onRegisterPlayback();
		// // resetTrack();
		// setUpPlayer()
		// addTrack();
		onRegisterPlayback();
		setUpPlayer()
		.then((response)=>{
			console.log("setUpPlayer response : ", response)
		})
		.catch((error)=>{
			console.log("setUpPlayer error : ", error)
		})
		addTrack(store_chaptarDetails?.prayer?.sound_file);

	})

	const fetch_PrayerChapter = (item)=>{
		const params={
      prayer_id:item?.id
    }
		api_postWithToken('user/prayer/chapters',params,access_token)
    .then((response) =>{
      showShowSkleton(false)
      if(response.status == "success"){
        // console.log("<======== user/prayer/chapters ======>", response.data)
        if(response?.data){
					dispatch(set_store_chaptarDetails(response?.data))
        }
      }
    })
    .catch((error) => {
      showShowSkleton(false);
      HelperFunctions.showToastMsg(JSON.stringify(error))
    })
	}

  const showFlatlistData =(data)=>{
    return (
      <View style={{marginTop:10}}>
        <Image
          style={{
              width: Dimensions.get('window').width-20,
              alignSelf: 'center',
              height: 200,
              borderRadius:5,
              // backgroundColor:'red'
          }}
          // source={AllSourcePath.LOCAL_IMAGES.forgetpass_logo}
          source={{ uri: data?.item }}
          resizeMode='cover'
      />
      {/* <Text>csdkbcskd </Text> */}
      </View>
    )
  }

	const showlistData =(data)=>{
		// console.log("cskdjbcskdckjsdbcdsjkcbsjkdc================================")
		// console.log(data.item)
    return (
      <View 
      // onPress={()=>{
			// 	// console.log(data?.item)
        
			// 	// let dataToSend = {chapter_id:item?.id}
			// 	// props.navigation.navigate('PrayerViewScreen',dataToSend)
      // }}
      >
      <View style={{
        flexDirection:'row',marginTop:15,
        borderColor:'#eb9800'
      }}>
      <View style={{flexDirection:'row'}}>
        <Text style={{flexWrap:'wrap',fontWeight:600,letterSpacing:0.5,paddingTop:5}}>{data?.item?.name}</Text>
        <Pressable onPress={()=>{
					navigateToSubScreen(data?.item)
				}}>
					<Text style={{flexWrap:'wrap',fontWeight:600,letterSpacing:0.5,color:'#eb9800',paddingRight:3}}>{data?.item?.id}</Text>
				</Pressable>
				</View>
    </View>
    </View>
    )
  }

	const navigateToSubScreen=(item)=>{
		// console.log("++++csdcdscdscsdcsdc========>:",item)
		if(item.prayer_lines_count == 0){
			HelperFunctions.showToastMsg('No prayer available in this chapter')
		}else{
			stopTrack();
			resetTrack();
			setisPlayTrack(false);

			let dataToSend = {chapter_id:item?.id}
			props.navigation.navigate('PrayerViewScreen',dataToSend)
		}

	}

	const func_StartOrGoButton=()=>{
		stopTrack();
		resetTrack();
		setisPlayTrack(false);
		// console.log(prayerNo)
		// console.log("cdsc ====== >",store_chaptarDetails?.chapters[123])
		if(prayerNo == ""){
			let chapterDetails = store_chaptarDetails?.chapters[0]
				// chapter_id:value.id
				const dataToSend = {chapter_id:chapterDetails?.id}
				props.navigation.navigate('PrayerViewScreen',dataToSend)
		}else{
			if(prayerNo !=0){
				
				// if(store_chaptarDetails?.chapters[prayerNo-1]){
				// 	let chapterDetails = store_chaptarDetails?.chapters[prayerNo-1]
				// 	// chapter_id:value.id
				// 	const dataToSend = {chapter_id:chapterDetails?.id}
				// 	props.navigation.navigate('PrayerViewScreen',dataToSend)
	
				// }else{
				// 	console.log("Not found");
				// 	console.log(store_chaptarDetails?.chapters)
				// }



				let found = false;
				for(let i=0;i<store_chaptarDetails?.chapters.length;i++){
					// if(store_chaptarDetails?.chapters[i].id == prayerNo){   //This line is disable
					if(store_chaptarDetails?.chapters[i].serial == prayerNo){
						if(store_chaptarDetails?.chapters[i].prayer_lines_count == 0){
							found = true;
						}
					}
				}
				if(found){
					HelperFunctions.showToastMsg('No prayer available in this chapter')
				}else{
					if(parseInt(prayerNo) <= store_chaptarDetails?.chapters.length && parseInt(prayerNo) > 0){
						// let value = parseInt(prayerNo); //This line is disable
						let index = parseInt(prayerNo);   //New line added
						let actIndex = index-1;                       //New line added
						// console.log("index value : ", index)          //New line added
						let value = store_chaptarDetails?.chapters[actIndex].id; //New line added
						const dataToSend = {chapter_id:value}
						props.navigation.navigate('PrayerViewScreen',dataToSend)
					}else{
						// console.log("csjbcskdjcbdsjkcbskjcbskcbdkds")
					}
				}


			}





			
		}
	}

	return (
		<ScreenLayout
			isScrollable={false}
			LeftComponent={''}
			// layoutBackground={{ backgroundColor: '#fff' }}
			layoutBackground={'#eb9800'}
			headerStyle={{ backgroundColor: '#eb9800' }}
			hideCustomHeader={false}
			viewStyle={{ backgroundColor: 'white', 
			// paddingHorizontal: 10 
		}}
			customStatusbarColor={"#ff0000"}
			showLoading={showloading}
			hideLeftComponentView={false}
			// leftComponentOnPress={() => props.navigation.goBack()}
			leftComponentOnPress={() => {
				stopTrack();
				props.navigation.goBack()
			}}
			hideRightComponentView={true}
			LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Prayer</Text>}
			IconColor={'#fff'}
		>
			{showSkleton ?  
          <CustomScleton /> 
        : 
        <View style={{flex:1}}>
          <View>
              <FlatList
                // horizontal
                // style={{width:100,backgroundColor:'red'}}
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                renderItem={showFlatlistData}
                data={[store_chaptarDetails?.prayer?.cover_image]}
                keyExtractor={(item, index) => index.toString()}
              />
          </View>
          
          <ScrollView showsVerticalScrollIndicator={false} style={{flex:1,marginHorizontal:10,marginBottom:10}}>
              <FlatList
                // pagingEnabled={true}
								horizontal
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                renderItem={showlistData}
                data={store_chaptarDetails?.chapters}
                keyExtractor={(item, index) => index.toString()}
								contentContainerStyle={{
									flexDirection: 'row',
									width: '100%',
									flexWrap: 'wrap',
									marginHorizontal:5
								}} 
              />
							{store_chaptarDetails?.chapters?.length >0 ?
							<View style={{marginTop:30,flexDirection:'row',justifyContent:'center'}}>
								<CustomButtonLeftIcon
									buttonText={"Play as audio"}
									customIconName={ isPlayTrack ? "pause-circle-outline": "play-circle-outline"} 
									customIconStyle={{fontSize:20 }}
									style={{paddingHorizontal:10,height:40,width:150}}
									onPress={() => { 
										if(isPlayTrack){
											pauseTrack()
											setisPlayTrack(false)
										}else{
											playTrack();
											setisPlayTrack(true)
										}
									}}
								/>	
							</View>
							:null}
          </ScrollView>
					{store_chaptarDetails?.chapters?.length >0 ?
					<View style={{}}>
						<View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginVertical:10}}>
							<View style={{flex:1}}>
								<EditTextComponent
										style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#B8B8B8',width:'60%',height:40 }}
										placeholder={"Start at a number"}
										secureTextEntry={false}
										keyboardType={'numeric'}
										value={prayerNo}
										onChangeText={(textValue) => { setprayerNo(textValue) }}
								/>
							</View>
							<View>
								<CustomButton
										buttonText={prayerNo == '' ? "Start over" : "Go"}
										customIconName="arrow-up"
										customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
										style={{paddingHorizontal:10,height:40}}
										onPress={() => { func_StartOrGoButton() }}
								/>
							</View>
						</View>
						
					</View>
					:null}

        </View>
      }
		</ScreenLayout>

	)
}

export default PrayerSubpageScreen

