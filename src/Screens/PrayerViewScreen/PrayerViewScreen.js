import { Alert, Dimensions, FlatList, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import { useDispatch, useSelector } from 'react-redux'
import { api_postWithToken } from '../../Services/Service'
import { set_store_chaptarDetails } from '../../Store/Reducers/CommonReducer'
import CustomScleton from '../../Components/CustomScleton/CustomScleton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import CustomButton from '../../Components/CustomButton/CustomButton'
import CustomButtonLeftIcon from '../../Components/CustomButtonLeftIcon/CustomButtonLeftIcon'
import RenderHTML from 'react-native-render-html'

import { onRegisterPlayback,addTrack, setUpPlayer, pauseTrack, playTrack, stopTrack, resetTrack } from '../../Services/TrackPlayerService'

const styles = Style()

const PrayerViewScreen = (props) => {
	const isFocused = useIsFocused()
	const dispatch = useDispatch()

	const [showloading, setShowloading] = useState(false)
	const [showSkleton, setShowSkleton] = useState(false);
	const [prevScreenParam, setprevScreenParam] = useState(null)

	const [prayerNo, setprayerNo] = useState(null)

	const [chapterId, setchapterId] = useState(null)
	const [lineDetails, setlineDetails] = useState(null)
	const [lineDesc, setlineDesc] = useState("<p>csbcksdbckjsdbcds</p>")

	const [isPlayTrack, setisPlayTrack] = useState(false)
	
	

	const { access_token, store_chaptarDetails } = useSelector(state => state.commonData);

	const { chapter_id } = props?.route?.params;

	useEffect(() => {
		// onRegisterPlayback();
		// setUpPlayer()

		stopTrack();
		resetTrack();
		setisPlayTrack(false);

		if(chapter_id){
			// console.log("chapter id112:", chapter_id)
				setchapterId(chapter_id);
				let params={
					chapter_id:chapter_id
				}
				fetch_chapterQuotes(params,'initial')
		}
	}, [chapter_id])

	useEffect(() => {
		console.log("cskjbcksjdckjsdc ===>")
		// onRegisterPlayback();
		// setUpPlayer()
		// setTimeout(() => {
		// 	addTrack(lineDetails?.prayer_line?.sound_file);
		// }, 100);

	}, [lineDetails])


	// {chapter_id: 1, line_id: 2}
	const fetch_chapterQuotes = (params,loadingType)=>{
		// console.log("Chapter ID12312321 : ",params)
		if(loadingType == 'initial'){
			setShowSkleton(true)
		}else if (loadingType == 'loader'){
			setShowloading(true)
		}
		
		api_postWithToken('user/prayer/line/details',params,access_token)
			.then((response) =>{
				if(loadingType == 'initial'){
					setShowSkleton(false)
				}else if (loadingType == 'loader'){
					setShowloading(false)
				}


				if(response.status == "success"){
					// console.log("<======== line/details======> <======== line/details======> <======== line/details======>")
					// console.log("<======== line/details======>", response?.data?.prayer_line?.sound_file);
					// console.log("<======== line/details======> <======== line/details======> <======== line/details======>")
					
					if(response?.data){
						// dispatch(set_store_chaptarDetails(response?.data))
						const data = response.data;
						setlineDetails(response?.data);
						addTrack(response?.data?.prayer_line?.sound_file)
						// if(data?.prayer_line?.description){
						// 	setlineDesc(data?.prayer_line?.description)
						// }

					}
				}
			})
			.catch((error) => {
				if(loadingType == 'initial'){
					setShowSkleton(false)
				}else if (loadingType == 'loader'){
					setShowloading(false)
				}
				HelperFunctions.showToastMsg(JSON.stringify(error))
			})
	}

  const showFlatlistData =(data)=>{
    return (
      <View style={{marginTop:10}}>
        <Image
          style={{
              width: Dimensions.get('window').width-20,
              alignSelf: 'center',
              height: 200,
              borderRadius:5,
              // backgroundColor:'red'
          }}
          // source={AllSourcePath.LOCAL_IMAGES.forgetpass_logo}
          source={{ uri: data?.item }}
          resizeMode='cover'
      />
      </View>
    )
  }

	// == CHnage slide function
	const func_changeSlide =(slideTo)=>{
		setlineDetails(null);
		if(slideTo == 'prev'){
			fetch_chapterQuotes(lineDetails?.navigation?.prev,'loader')
		}else if (slideTo == 'next'){
			fetch_chapterQuotes(lineDetails?.navigation?.next,'loader')
		}
	}

	return (
		<ScreenLayout
			isScrollable={false}
			LeftComponent={''}
			// layoutBackground={{ backgroundColor: '#fff' }}
			layoutBackground={'#eb9800'}
			headerStyle={{ backgroundColor: '#eb9800' }}
			hideCustomHeader={false}
			viewStyle={{ backgroundColor: 'white', 
			// paddingHorizontal: 10 
		}}
			customStatusbarColor={"#ff0000"}
			showLoading={showloading}
			hideLeftComponentView={false}
			leftComponentOnPress={() => {
				stopTrack();
				resetTrack();
				setisPlayTrack(false);
				props.navigation.goBack()
			}}
			hideRightComponentView={true}
			LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Prayer details</Text>}
			IconColor={'#fff'}
		>
			{showSkleton ?  
          <CustomScleton /> 
        : 
        <View style={{flex:1}}>
          <View>
              <FlatList
                // horizontal
                // style={{width:100,backgroundColor:'red'}}
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                renderItem={showFlatlistData}
                data={[lineDetails?.chapter?.cover_image]}
                keyExtractor={(item, index) => index.toString()}
              />
          </View>
          
          <ScrollView showsVerticalScrollIndicator={false} style={{flex:1,marginHorizontal:10,marginBottom:10}}>
							<View style={{marginTop:20}}>
								<Text style={{textAlign:'center',fontSize:20,letterSpacing:0.5,fontWeight:500}}>
									{lineDetails?.chapter?.name}
									</Text>
									<Text style={{textAlign:'center',fontSize:20,letterSpacing:0.5,fontWeight:500}}>
									{lineDetails?.chapter?.id}
									</Text>
							</View>
							<View>
								{/* <Text style={{textAlign:'center',fontSize:16,letterSpacing:0.5}}>
									{lineDetails?.prayer_line?.description}
									</Text> */}
									{/* {
									console.log("lineDetails?.prayer_line?.description : ", lineDetails?.prayer_line?.description)
									// lineDetails?.prayer_line?.description ? 
									//  <RenderHTML html={lineDetails?.prayer_line?.description} />
									//  :null
									 } */}
									 {/* {lineDetails?.prayer_line?.description ?  */}

									 {lineDetails?.prayer_line?.description ? 
									 <RenderHTML 
									 width={'100%'}
									 source={{html:`<div>${lineDetails?.prayer_line?.description}</div>`}} />
									:null}

									{/* <RenderHTML source={`<div>${lineDetails?.prayer_line?.description}</div>`} /> */}
									 
							</View>
							{lineDetails?.prayer_line?.sound_file ?
							<View style={{marginTop:30,flexDirection:'row',justifyContent:'center'}}>
								<CustomButtonLeftIcon
									buttonText={"Play as audio"}
									// customIconName="play-circle-outline"
									customIconName={ isPlayTrack ? "pause-circle-outline": "play-circle-outline"} 
									customIconStyle={{ fontSize:20 }}
									style={{paddingHorizontal:10,height:40,width:150}}
									// onPress={() => { console.log(lineDetails?.prayer_line?.sound_file)}}
									onPress={() => { 
										if(isPlayTrack){
											pauseTrack()
											setisPlayTrack(false)
										}else{
											playTrack();
											setisPlayTrack(true)
										}
									}}
								/>	
							</View>
							:null}
          </ScrollView>
					{lineDetails?.navigation?.next !=null || lineDetails?.navigation?.prev !=null ?
					<View style={{backgroundColor:'#020202'}}>
						<View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10,marginVertical:10}}>
							
							<View style={{flex:1,flexDirection:'row',justifyContent:'flex-start'}}>
								{lineDetails?.navigation?.prev !=null?
								<CustomButtonLeftIcon
										buttonText={'Prev'}
										customIconName="arrow-back-outline"
										customIconStyle={{ 
											// transform: [{ rotateZ: '45deg' }] 
											color:'#eb9800'
										}}
										buttonTextStyle={{color:'#eb9800'}}
										style={{paddingHorizontal:10,height:40,width:100,backgroundColor:'#020202'}}
										onPress={() => { 
											stopTrack();
											resetTrack();
											setisPlayTrack(false)
											func_changeSlide('prev') 
										}}
								/>
								:null}
							</View>
							<View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
							{lineDetails?.navigation?.next !=null?
								<CustomButton
										buttonText={'Next'}
										customIconName="arrow-forward-outline"
										customIconStyle={{ 
											// transform: [{ rotateZ: '45deg' }] 
											color:'#eb9800'
										}}
										buttonTextStyle={{color:'#eb9800'}}
										style={{height:40,width:100,backgroundColor:'#020202'}}
										onPress={() => { 
											stopTrack();
											resetTrack();
											setisPlayTrack(false)
											func_changeSlide('next')
										 }}
								/>
								:null}
							</View>
						</View>
						
					</View>
					:null}

        </View>
      }
		</ScreenLayout>

	)
}

export default PrayerViewScreen

