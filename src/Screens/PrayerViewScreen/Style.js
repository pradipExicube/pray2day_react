import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Theme from '../../Constants/Theme'

const Style = () => {

  return StyleSheet.create({
    topHeading:{marginTop:(Theme.windowHeight*0.11),textAlign:'center',fontSize:Theme.sizes.s16,color:Theme.colors.white,fontFamily:Theme.FontFamily.medium},
    sunHeadingSvgStyle:{alignSelf:'center',marginTop:36}
  })

}

export default Style
