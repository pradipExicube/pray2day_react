import { Dimensions, FlatList, Image, ImageBackground, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AllSourcePath from '../../Constants/PathConfig'
import Theme from '../../Constants/Theme'
import JesusMaryIcon from '../../assets/icons/JesusMaryIcon'
import Style from './Style'
import { useIsFocused } from '@react-navigation/native'
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout'
import CustomButton from '../../Components/CustomButton/CustomButton'
import EditTextComponent from '../../Components/EditTextComponent/EditTextComponent'
import MenuIcon from '../../assets/icons/MenuIcon'
import { api_postWithToken, api_postWithoutToken, demoStripeAPi } from '../../Services/Service'
import HelperFunctions from '../../Constants/HelperFunctions'

import { PlatformPay, initStripe, useStripe } from '@stripe/stripe-react-native';
import { useDispatch, useSelector } from 'react-redux'
import { set_Contry, set_subscriptionDetails, set_subscriptionList } from '../../Store/Reducers/CommonReducer'
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/Ionicons';
import CustomDropdown from '../../Components/CustomDropdown/CustomDropdown'

// import { useIAP, requestPurchase, isIosStorekit2, PurchaseError, withIAPContext } from 'react-native-iap';



const styles = Style()

// ===============================================================================================
// import * as RNIap from 'react-native-iap';

// const itemSkus = Platform.select({
//   ios: ['com.techinvein.prayertoday.monthly30'],
//   // android: ['com.example.subscription1', 'com.example.subscription2'],
// });
// useState(()=>{
//   RNIap.initConnection().catch(() => {
//     console.log('Error connecting to store');
//   });
// },[])

// ===============================================================================================




const SubscriptionplanScreen = (props) => {
  const isFocused = useIsFocused()
  const dispatch = useDispatch()

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const [eyevalue, setEyevalue] = useState(true)
  const [showloading, setShowloading] = useState(false)
  const [showTick, setShowTick] = useState(false)
  const [stripeRegistration, setStripeRegistration] = useState(false)
  const [showAddressMondal, setshowAddressMondal] = useState(false)

  const [countryList, setCountryList] = useState([])
  const [selectedCountry, setSelectedCountry] = useState(null)
  const [stateList, setStateList] = useState([])
  const [selectedState, setSelectedState] = useState(null)
  const [city, setCity] = useState("")
  const [address1, setAddress1] = useState("")
  const [address2, setAddress2] = useState("")
  const [zip, setZip] = useState("")

  const { access_token, user_details, subscriptionDetails, api_subscriptionList, store_countryList } = useSelector(state => state.commonData);

  const [currentIndex, setCurrentIndex] = useState(null)
  const [currentSlectedPlan, setCurrentSlectedPlan] = useState({})

  const { initPaymentSheet, presentPaymentSheet } = useStripe();


 
  useEffect(() => {
    setCurrentIndex(null);
    setCurrentSlectedPlan({})

    initStripe({
      // publishableKey: 'pk_test_51J3baoSBUUFJKWGU9pW4G46U6JXYLSU4H3YSUI7HmEDe0gqjjUU2EmIp9E4hitZRoqzdXDFkNBpi5RcunqQKQqOV002a7YRnWC',
      publishableKey:'pk_live_ErIzehghjZhw43nFp1j7sa8L00YWLydRxM',// publishable key'
      merchantIdentifier: 'merchant.com.techinvein.prayertoday'
    });

    func_fetch_subscriptionList()

  }, [])

  useEffect(() => {
    // api_fetchCOuntry()
    // console.log("access_token :", access_token)
  })

  const api_fetchCOuntry = () => {
    setShowloading(true)
    api_postWithoutToken('master/countries', {})
      .then((response) => {
        setShowloading(false)
        // console.log("'master/countries ===>", JSON.stringify(response));
        if (response.status == "success") {
          let data = response.data
          if (data?.countries) {
            // set_Contry(data?.countries)
            setCountryList(data?.countries)
            setTimeout(() => {
              setshowAddressMondal(true)
            }, 100);
            // console.log("csdbcksdbc => ", data?.countries)

            // setTimeout(() => {
            //   console.log("countryList ==>", countryList);
            // }, 5000);
          } else {
            // set_Contry([])
            setCountryList([])
          }
        } else {
          // set_Contry([])
          setCountryList([])
        }
      })
      .catch((error) => {
        // set_Contry([])
        setShowloading(false)
        HelperFunctions.showToastMsg(JSON.stringify(error));
      })
  }


  // == Fetch subscription list func
  const func_fetch_subscriptionList = () => {
    // if(api_subscriptionList?.length == 0){
    api_callSubscriptionList()
    // }
  }

  // == API subscription list
  const api_callSubscriptionList = () => {
    setShowloading(true)
    api_postWithToken('user/subscription/fetch-plans', {}, access_token)
      .then((response) => {
        setShowloading(false)
        // console.log("response", JSON.stringify(response));
        if (response.status == "success") {
          const sdata = response.data;
          setStripeRegistration(sdata?.stripe_registration)
          if (sdata.plans) {
            console.log("subscription ====== >cshbcsdb", JSON.stringify(sdata.plans));
            dispatch(set_subscriptionList(sdata.plans))
            // setTimeout(() => {
            //   console.log("subscription lists : ", api_subscriptionList);
            // }, 3000);
          } else {
            console.log("Already present subscription")
            dispatch(set_subscriptionList([]))
          }

        }
      })
      .catch((err) => {
        setShowloading(false)
        HelperFunctions.showToastMsg(JSON.stringify(err));
      })
  }

  const checkFUnc = (item) => {
    // console.log("currentIndex == >", currentIndex)
    // console.log("currentSlectedPlan == >", item)
    setShowloading(true)
    if (item) {
      const n_value = parseFloat(item?.price)
      // demoStripeAPi(n_value).then(response => {
        let params={
          payment_mode:"stripe",
          plan_id:item?.id
        }
      api_postWithToken('user/subscription/purchase',params,access_token)
      .then(response => {
        setShowloading(false);
        if (response?.status == 'success') {
          if (response?.data) {
            if(response?.data?.payment_intent){
              initializePaymentSheet(response?.data?.payment_intent?.client_secret, response?.data?.payment_intent?.id)
            }
          }
        }else{
          HelperFunctions.showToastMsg(response?.message)
        }

      }).catch(error => {
        HelperFunctions.showToastMsg(JSON.stringify(error))
      }).finally(() => {
      })
    } else {
      setShowloading(false);
      HelperFunctions.showToastMsg("Kindly select plan, before proceeding...")
    }
  }



  const initializePaymentSheet = (clientId, paymentId) => {
    console.log(clientId, paymentId);
    initPaymentSheet({
      merchantDisplayName: 'Pray2Day',
      paymentIntentClientSecret: clientId,
      allowsDelayedPaymentMethods: true,
      returnURL: 'your-app://stripe-redirect',
      applePay:false,
      defaultBillingDetails: {
        address: {
          country: "US",
        },
      },
      applePay: {
        merchantCountryCode: "US",
        currencyCode: 'USD',
      },

      appearance: {
        primaryButton: {
          colors: {
            background: Theme.colors.primary,
          },
        },
      },
    }).then(response => {
      // console.log("paymentId response : ", response)
      openPaymentSheet(paymentId)
    }).catch(error => {
      // console.log("paymentId error : ", response)
      changeloadingState(false)
    })

  };


  const openPaymentSheet = (paymentId) => {
    // changeloadingState(false)
    presentPaymentSheet().then(response => {
      if (response.error) {
        // console.log("openPaymentSheet error ==> ", response.error)
        HelperFunctions.showToastMsg(response.error.message)
      } else {
        // console.log("ABC =======>", response?.success);
        completeTran(paymentId)
      }

    }).catch(error => {
      // console.log("openPaymentSheet catch error ==> ", error)
      HelperFunctions.showToastMsg(error?.message ? error?.message : "Unknown Error")
    })


  };

  const completeTran = (paymentId) => {
    console.log("Payment complete ==== > 123" + paymentId)
    if(paymentId){
      api_fetchMySubscription()
    }
  }

  const api_fetchMySubscription =()=>{
    setShowloading(true);
    api_postWithToken('user/subscription/active-plan', {}, access_token).then(response => {
        setShowloading(false)
        if (response.status == 'success') {
        const sub_data = response.data;
        if (sub_data?.subscription) {
          console.log("subscription ", subscriptionDetails);
          dispatch(set_subscriptionDetails(sub_data?.subscription));
          setTimeout(() => {
            props.navigation.replace('HomeScreen');
          }, 500);

        } 
      } else {
        HelperFunctions.showToastMsg(response.message)
      }

    }).catch(error => {
        setShowloading(false);
        HelperFunctions.showToastMsg(JSON.stringify(error));
    })
  }




  const showFlatlistData = (data) => {
    // console.log("csdkjbcksdjbcjkdsbcdkbsc : " , data)
    return (
      <TouchableOpacity
        onPress={() => {
          
          if(stripeRegistration  == true){
            checkFUnc(data.item);
          }else{
            setCurrentIndex(data.item.id);
            setCurrentSlectedPlan(data.item)
            setSelectedCountry(null)
            setCountryList([])
            setStateList([]);
            setSelectedState(null)
            api_fetchCOuntry();
          }

          // handleApplePay()
          

          // buySubscription('monthly30')


        }}
        style={{
          height: 120, backgroundColor: 'rgb(255, 247, 222)',
          borderWidth: 0.3, borderColor: '#eb9800', borderRadius: 12,
          justifyContent: 'space-around', marginVertical: 10
        }} duration
      >
        <Text style={{ textAlign: 'center' }}>{data?.item?.name} 
        {/* ({data?.item?.duration} Days) */}
        </Text>
        <Text style={{ textAlign: 'center', fontSize: 32, fontWeight: '600' }}>${data?.item?.price}</Text>
        {/* <Text style={{ textAlign: 'center' }}>{data?.item?.duration} Days</Text> */}
        <Text style={{ textAlign: 'center' }}>{data?.item?.description}</Text>
        
        <View style={{ position: 'absolute', right: 10, top: 10, height: 30, width: 30, borderRadius: 30, backgroundColor: '#fff', borderColor: '#eb9800', borderWidth: 1 }}>
          {currentIndex == data.item.id ?
            <Image
              style={{ height: 30, width: 30, borderRadius: 30 }}
              source={AllSourcePath.LOCAL_IMAGES.corrent_img}
            />
            : null}
        </View>
      </TouchableOpacity>
    )
  }

  const selectDropdown = (data) => {
    console.log("Working data : ", data);
    setSelectedCountry(data)
    HelperFunctions.showToastMsg("State fetching, Please wait...")
    setStateList([]);
    setSelectedState(null)
    api_fetchState(data);
  }

  const api_fetchState = (value) => {
    // setShowloading(true);
    let params = {
      country_id: value?.id
    }
    api_postWithoutToken('master/states', params)
      .then((response) => {
        // setShowloading(false)
        if (response.status == "success") {
          let data = response?.data
          console.log("State data : ", data)
          if (data?.states?.length > 0 ) {
            setStateList(data?.states)
          } else {
            setStateList([])
            HelperFunctions.showToastMsg("State not found, please select valid country")
          }
        } else {
          setStateList([])
        }
      })
      .catch((error) => {
        // setShowloading(false)
        HelperFunctions.showToastMsg("State list error : ", JSON.stringify(error))
      })
  }

  const selectState = (data) => {
    console.log("Working data : ", data);
    // setSelectedState(null)
    setSelectedState(data)
    // api_fetchState(data);
  }


  //FUnction confirm and pay ==>
  const func_confirmNpay =()=>{
    if(!selectedCountry?.id){
      HelperFunctions.showToastMsg("Country can not be blank")
    }else if(!selectedState?.id){
      HelperFunctions.showToastMsg("State can not be blank")
    }else if(city == "" || city == null || city == undefined){
      HelperFunctions.showToastMsg("City can not be blank")
    }else if(address1 == "" || address1 == null || address1 == undefined){
      HelperFunctions.showToastMsg("Address line 1 can not be blank")
    }else if(address2 == "" || address2 == null || address2 == undefined){
      HelperFunctions.showToastMsg("Address line 2 can not be blank")
    }else if(zip == "" || zip == null || zip == undefined){
      HelperFunctions.showToastMsg("Zip code can not be blank")
    }else{
      let params={
        address_country_id:selectedCountry?.id,
        address_state_id:selectedState?.id,
        address_city:city,
        address_line1:address1,
        address_line2:address2,
        address_zip:zip
      }
      console.log("Address params == > ", params)
      start_registerToStripe(params)
    }
  }

  //Data registered to stripe function + APi ==>
  const start_registerToStripe =(params)=>{
    setShowloading(true);
    api_postWithToken('user/subscription/stripe/register',params,access_token)
    .then((response)=>{
      setShowloading(false);
      console.log("user/subscription/stripe/register = ", response);
      if(response.status == 'success'){
        checkFUnc(currentSlectedPlan);
      }else{
        HelperFunctions.showToastMsg(response?.message)
      }
    })
    .catch((error)=>{
      setShowloading(false);
      console.log("registered error : ", JSON.stringify(error));
    })

  }

  return (
    <ScreenLayout
      isScrollable={false}
      LeftComponent={''}
      // layoutBackground={{ backgroundColor: '#fff' }}
      layoutBackground={'#eb9800'}
      IconColor={'#fff'}
      headerStyle={{ backgroundColor: '#eb9800' }}
      hideCustomHeader={false}
      viewStyle={{ backgroundColor: 'white', paddingHorizontal: 30 }}
      customStatusbarColor={"#ff0000"}
      showLoading={showloading}
      hideLeftComponentView={true}
      leftComponentOnPress={() => props.navigation.goBack()}

      // hideMiddleComponentView={true}
      hideRightComponentView={false}
      rightComponent={
        <TouchableOpacity
          style={{}}
          onPress={() => { props.navigation.navigate("MenuScreen") }}
        >
          <MenuIcon
            menuIconColor={'#fff'}
            style={{ alignSelf: 'center', padding: 10, color: '#fff' }} />
        </TouchableOpacity>

      }
      // rightComponentOnPress={() => {props.navigation.navigate("MenuScreen")}}

      LeftComponentextrat={<Text style={{ fontSize: 16, color: '#fff' }}>Subscription Plans</Text>}
    >


      <FlatList
        // style={{ alignSelf: 'center', width: '100%' }}
        renderItem={showFlatlistData}
        data={api_subscriptionList}
        keyExtractor={(item, index) => index.toString()}
      />


      <View>
        <Modal isVisible={showAddressMondal}
          // animationType="slide"
          // transparent={true}
          onBackdropPress={() => {
            setshowAddressMondal(false)
          }}
        >
          <View style={{
            height: '80%', width: Dimensions.get('window').width,
            marginTop: 'auto', marginBottom: -20,
            backgroundColor: 'white', alignSelf: 'center',
            borderTopEndRadius: 10, borderTopLeftRadius: 10

          }}
          >
            <View style={{ backgroundColor: '#E7E8E9', borderTopEndRadius: 10, borderTopLeftRadius: 10 }}>
              <Text
                style={{
                  fontSize: 16, paddingVertical: 10, paddingHorizontal: 20,letterSpacing: 1, fontWeight: 500,
                }}>
                Add your payment information
              </Text>
            </View>
            <View style={{ paddingHorizontal: 20, paddingVertical: 20, flex: 1 }}>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ paddingBottom: 10, flex: 1, justifyContent: 'center' }}>
                    <Text style={{ letterSpacing: 0.5 }}>Country*</Text>
                  </View>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <CustomDropdown
                      title={selectedCountry?.name ? selectedCountry?.name : "Selecct country"}
                      value={countryList}
                      onPress={selectDropdown}
                      ContainerStyle={{}}
                      ItemStyle={{ borderBottomWidth: 0 }}

                    />
                  </View>
                </View>
                {stateList?.length > 0 ?
                  <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <View style={{ paddingBottom: 10, flex: 1, justifyContent: 'center' }}>
                      <Text style={{ letterSpacing: 0.5 }}>State*</Text>
                    </View>
                    <View style={{ flex: 2, justifyContent: 'center' }}>
                      <CustomDropdown
                        title={selectedState?.name ? selectedState?.name : "Selecct State"}
                        value={stateList}
                        onPress={selectState}
                        ContainerStyle={{}}
                        ItemStyle={{ borderBottomWidth: 0 }}
                      />
                    </View>
                  </View>
                  : null
                }

                <View style={{ marginTop: 15, flex: 1 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <EditTextComponent
                      style={{ borderWidth: 0.5, justifyContent: 'center', borderColor: '#E7E8E9', width: '100%' }}
                      placeholder={"City*"}
                      value={city}
                      onChangeText={(textValue) => { setCity(textValue) }}
                    />
                  </View>
                </View>

                <View style={{ marginTop: 15, flex: 1 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <EditTextComponent
                      style={{ borderWidth: 0.5, borderColor: '#E7E8E9', width: '100%' }}
                      placeholder={"Address line 1*"} value={address1}
                      onChangeText={(textValue) => { setAddress1(textValue) }}
                    />
                  </View>
                </View>

                <View style={{ marginTop: 15, flex: 1 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <EditTextComponent
                      style={{ borderWidth: 0.5, borderColor: '#E7E8E9', width: '100%' }}
                      placeholder={"Address line 2*"}value={address2}
                      onChangeText={(textValue) => { setAddress2(textValue) }}
                    />
                  </View>
                </View>

                <View style={{ marginTop: 15, flex: 1 }}>
                  <View style={{ flex: 2, justifyContent: 'center' }}>
                    <EditTextComponent
                      style={{ borderWidth: 0.5, borderColor: '#E7E8E9', width: '100%' }}
                      placeholder={"ZIP Code*"} value={zip}
                      onChangeText={(textValue) => { setZip(textValue) }}
                    />
                  </View>
                </View>

              </ScrollView>

              <View style={{ marginBottom: 20 }}>
                <CustomButton
                  buttonText={currentSlectedPlan?.price ? `Confirm & Pay $ ${currentSlectedPlan.price}` : 'Confirm & Pay'}
                  customIconName="arrow-up"
                  customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
                  onPress={() => {
                    // console.log("currentSlectedPlan : ", currentSlectedPlan)
                    setshowAddressMondal(false);
                    // setTimeout(() => {
                      // checkFUnc(currentSlectedPlan);
                      func_confirmNpay()
                    // }, 500);
                  }}
                />
              </View>

            </View>
          </View>

        </Modal>
      </View>




      {/* <View style={{ paddingTop: 40 }}>
                <CustomButton
                    buttonText={"Reset Now"}
                    customIconName="arrow-up"
                    customIconStyle={{ transform: [{ rotateZ: '45deg' }] }}
                    onPress={() => {
                        props.navigation.navigate('ResetpassScreen')

                    }}
                />
            </View> */}


    </ScreenLayout>

  )
}

export default SubscriptionplanScreen
// export default withIAPContext(SubscriptionplanScreen)

