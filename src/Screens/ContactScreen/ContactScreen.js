import React, { useState } from 'react';
import { Dimensions, FlatList, ImageBackground, SafeAreaView, StatusBar, StyleSheet, Text, View, Image, useAnimatedValue, TouchableOpacity, TextInput, ScrollView, Platform, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AllSourcePath from '../../Constants/PathConfig';
import ScreenLayout from '../../Components/ScreenLayout/ScreenLayout';
import Rightarrow from '../../assets/icons/Rightarrow';

import Theme from '../../Constants/Theme';
import HelperFunctions from '../../Constants/HelperFunctions';
import { postApi } from '../../Services/Service';

const ContactScreen = (props) => {
    const [name, setName] = useState("")
    const [lname, setLname] = useState("")
    const [email, setEmail] = useState("")
    const [Message, setMessage] = useState("")

    const [phoneNumber, setPhoneNumber] = useState('')
    const [Loder, setLoader] = useState(false);

    const Submit = () => {
        if (name == null || name == undefined || name.trim().length == 0) {
            HelperFunctions.showToastMsg("Please enter name")
        } else if (lname == null || lname == undefined || lname.trim().length == 0) {
            HelperFunctions.showToastMsg("Please enter Last_name")
        } else if (email == null || email == undefined || email.trim().length == 0) {
            HelperFunctions.showToastMsg("Please enter email")
        }
        else if (!HelperFunctions.isvalidEmailFormat(email)) {
            HelperFunctions.showToastMsg("Please enter valid email")
        }
        else if (phoneNumber == null || phoneNumber == undefined || phoneNumber.trim().length == 0) {
            HelperFunctions.showToastMsg("Please enter phone number")
        }
        else if (phoneNumber.trim().length < 10) {
            HelperFunctions.showToastMsg("Phone number minimum have 10 digit")
        }
        else {
            setLoader(true)
            let data={
                "first_name":name,
                "last_name":lname,
                "email":email,
                "phone_number":phoneNumber,
                "message":Message
            }
            postApi("contact",data,"").then(response=>{
              // console.log('response',response)
              if (response?.status) {
                
              //   props.navigation.replace("DrawerNavigation")
                setLoader(false)
                HelperFunctions.showToastMsg("Message sent successfully")
                props.navigation.goBack()
                
              } else {
                HelperFunctions.showToastMsg(response?.error)
                setLoader(false)
  
              }
            }).catch(error=>{
                HelperFunctions.showToastMsg(error?.message)
              setLoader(false)
            }).finally(()=>{
              setLoader(false)
            })
      
          }
    }

    

    return (

        <ImageBackground style={{ flex: 1, }} source={AllSourcePath.LOCAL_IMAGES.SplashLayoutImage}>
            <StatusBar backgroundColor='transparent' showHideTransition={'slide'} barStyle={'dark-content'} hidden={false} translucent={true} networkActivityIndicatorVisible={true} />
            <SafeAreaView style={{
                marginTop: 20,
                flex: 1
            }}>
                <View style={{

                    flexDirection: "row",
                    marginBottom: "10%",
                    marginTop: 12
                }}>
                    <View style={{

                        padding: 12,
                        flex: 3
                    }}>
                        <Text style={{
                            alignSelf: "flex-end",
                            color: "#3A3A3C",
                            fontFamily: Theme.FontFamily.medium,
                            fontSize: 16
                        }}>CONTACT US</Text>
                    </View>
                    <View style={{

                        padding: 12,
                        flex: 2,
                    }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate("MenuScreen")}>
                            <Image source={require("../../assets/images/cross.png")}
                                style={{
                                    height: 24,
                                    width: 24,
                                    alignSelf: 'flex-end'
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView
                    contentContainerStyle={{
                        paddingBottom: 14
                    }}
                >
                    <View>
                        <Text style={{
                            color: "#3A3A3C",
                            fontSize: 14,
                            fontFamily: Theme.FontFamily.PlusJakartaSansMedium,
                            marginStart: "7%",
                            marginBottom: "10%"
                        }}>FILL UP THE FORM BELOW!</Text>
                        <View style={{
                            // height: 445,
                            width: 342,
                            alignSelf: "center",
                            borderRadius: 12,
                            backgroundColor: "#FFFFFF",
                            marginBottom: "12%",
                            paddingVertical: 10,

                        }}>
                            <View style={{
                                marginHorizontal: 20,
                                // marginTop: 17,
                                // backgroundColor:"red"
                            }}>

                                <View style={{
                                    width: "100%",
                                    borderBottomWidth: 1,
                                    borderColor: "#C1C0C0",
                                    paddingVertical: Platform.OS === 'ios' ? 12 : 3,
                                    marginBottom: Platform.OS === 'ios' ? 12 : 3
                                }}>
                                    <TextInput
                                        placeholder='FIRST NAME'
                                        placeholderTextColor={"#C1C0C0"}
                                        value={name}
                                        onChangeText={(text) => setName(text)}
                                        style={{
                                            fontSize: 16,
                                            fontFamily: Theme.FontFamily.medium,

                                        }}
                                    />
                                </View>
                                <View style={{
                                    width: "100%",
                                    borderBottomWidth: 1,
                                    borderColor: "#C1C0C0",
                                    paddingVertical: Platform.OS === 'ios' ? 12 : 3,
                                    marginBottom: Platform.OS === 'ios' ? 12 : 3
                                }}>
                                    <TextInput
                                        placeholder='LAST NAME'
                                        placeholderTextColor={"#C1C0C0"}
                                        value={lname}
                                        onChangeText={(text) => setLname(text)}
                                        style={{
                                            fontSize: 16,
                                            fontFamily: Theme.FontFamily.medium,

                                        }}
                                    />
                                </View>
                                <View style={{
                                    width: "100%",
                                    borderBottomWidth: 1,
                                    borderColor: "#C1C0C0",
                                    paddingVertical: Platform.OS === 'ios' ? 12 : 3,
                                    marginBottom: Platform.OS === 'ios' ? 12 : 3
                                }}>
                                    <TextInput
                                        placeholder='EMAIL ADDRESS'
                                        placeholderTextColor={"#C1C0C0"}
                                        value={email}
                                        onChangeText={(text) => setEmail(text)}
                                        style={{
                                            fontSize: 16,
                                            fontFamily: Theme.FontFamily.medium,

                                        }}
                                    />
                                </View>
                                <View style={{
                                    width: "100%",
                                    borderBottomWidth: 1,
                                    borderColor: "#C1C0C0",

                                    paddingVertical: Platform.OS === 'ios' ? 12 : 3,
                                    marginBottom: Platform.OS === 'ios' ? 12 : 10
                                }}>
                                    <TextInput
                                        placeholder='PHONE NUMBER'
                                        placeholderTextColor={"#C1C0C0"}
                                        value={phoneNumber}
                                        maxLength={15} 
                                        onChangeText={(text) => setPhoneNumber(text.replace(/[^0-9]/g, ''))}
                                        
                                        keyboardType='phone-pad'
                                        style={{
                                            fontSize: 16,
                                            fontFamily: Theme.FontFamily.medium,

                                        }}
                                    />
                                </View>

                               
                                    <View
                                        style={{
                                            width: "100%",
                                            borderBottomWidth: 1,
                                            borderColor: "#C1C0C0",
                                            marginBottom: 12,
                                            // backgroundColor:"red"
                                            height: 100
                                        }}>
                                        <TextInput
                                            editable
                                            multiline
                                            // numberOfLines={4}
                                            value={Message}
                                            onChangeText={(val)=>setMessage(val)}
                                            maxLength={300}
                                            placeholder='MESSAGE'
                                            placeholderTextColor={"#C1C0C0"}
                                            style={{
                                                fontSize: 16,
                                                fontFamily: Theme.FontFamily.medium,
                                                // paddingBottom: 40
                                            }}
                                        />
                                    </View>
                                

                            </View>
                        </View>
                    </View>


                    <TouchableOpacity
                        onPress={() => Submit()}
                        disabled={Loder}
                        style={{
                            height: 60,
                            width: 342,
                            backgroundColor: "#3964AE",
                            borderRadius: 12,
                            alignSelf: "center",
                            justifyContent: "center",

                        }}

                    >
                        <Text style={{
                            textAlign: "center",
                            color: "#FFFFFF",
                            fontSize: 16,
                            fontFamily: Theme.FontFamily.medium,
                            letterSpacing: 2
                        }}>SEND MESSAGE</Text>
                        {Loder ?
                                    <ActivityIndicator style={{ marginHorizontal: 10 }} color={"#fff"} size={20} />
                                    :
                                    null
                                        }
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        </ImageBackground>
    )
}

export default ContactScreen;
