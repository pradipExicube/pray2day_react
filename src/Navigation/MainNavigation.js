import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { ContactScreen, EditprofileScreen, ForgotpassScreen, HomeScreen, IntroScreen, LoginScreen, MenuScreen, MySubscriptionScreen, OtpScreen, PaymentHistoryScreen, PrayerSubpageScreen, PrayerViewScreen, PrivacyScreen, ResetpassScreen, SplashScreen, SubscriptionplanScreen, TermsScreen } from '../Screens';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import ProviderScreen from '../Screens/ProviderScreen/ProviderScreen';
import AboutUs from '../Screens/About/AboutUs';
import NewsletterIndex from '../Screens/Newsletter/NewsletterIndex';
import SignupScreen from '../Screens/SignupScreen/SignupScreen';




const Stack = createNativeStackNavigator();

function MainNavigation() {
  return (
    
    <Stack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: "#ffffff",
        // height: 100,
      },
      
    }}>

    <Stack.Screen
        name="SplashScreen"
        options={{headerShown: false}}
        component={SplashScreen}
      />
    <Stack.Screen
        name="IntroScreen"
        options={{headerShown: false}}
        component={IntroScreen}
    />
    <Stack.Screen
        name="LoginScreen"
        options={{headerShown: false}}
        component={LoginScreen}
    />
    <Stack.Screen
        name="SignupScreen"
        options={{headerShown: false}}
        component={SignupScreen}
    />

    <Stack.Screen
        name="OtpScreen"
        options={{headerShown: false}}
        component={OtpScreen}
    />

    <Stack.Screen
        name="ForgotpassScreen"
        options={{headerShown: false}}
        component={ForgotpassScreen}
    />
    <Stack.Screen
        name="ResetpassScreen"
        options={{headerShown: false}}
        component={ResetpassScreen}
    />
    <Stack.Screen
        name="SubscriptionplanScreen"
        options={{headerShown: false}}
        component={SubscriptionplanScreen}
    />
    <Stack.Screen
        name="MySubscriptionScreen"
        options={{headerShown: false}}
        component={MySubscriptionScreen}
    />
    <Stack.Screen
        name="PaymentHistoryScreen"
        options={{headerShown: false}}
        component={PaymentHistoryScreen}
    />
    <Stack.Screen
        name="PrivacyScreen"
        options={{headerShown: false}}
        component={PrivacyScreen}
    />
    <Stack.Screen
        name="TermsScreen"
        options={{headerShown: false}}
        component={TermsScreen}
    />
    <Stack.Screen
        name="PrayerSubpageScreen"
        options={{headerShown: false}}
        component={PrayerSubpageScreen}
    />
    <Stack.Screen
        name="PrayerViewScreen"
        options={{headerShown: false}}
        component={PrayerViewScreen}
    />









    <Stack.Screen
        name="EditprofileScreen"
        options={{headerShown: false}}
        component={EditprofileScreen}
    />

    






    





    <Stack.Screen
        name="HomeScreen"
        options={{headerShown: false}}
        component={HomeScreen}
      />


<Stack.Screen
        name="MenuScreen"
        options={{headerShown: false}}
        component={MenuScreen}
      />

<Stack.Screen
        name="ContactScreen"
        options={{headerShown: false}}
        component={ContactScreen}
      />
<Stack.Screen
        name="ProviderScreen"
        options={{headerShown: false}}
        component={ProviderScreen}
      />
      <Stack.Screen
        name="AboutUs"
        options={{headerShown: false}}
        component={AboutUs}
      />
            <Stack.Screen
        name="NewsletterIndex"
        options={{headerShown: false}}
        component={NewsletterIndex}
      />

    


      
    </Stack.Navigator>
    
    
  );
}

export default MainNavigation;
