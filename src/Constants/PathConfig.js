const AllSourcePath = {
    API_BASE_URL_DEV: "https://pray2day.ca/api/",
    API_IMG_URL_DEV: "",
    API_BASE_URL_LIVE: "",
    API_IMG_URL_LIVE: "",
    LOCAL_IMAGES: {
    SplashLayoutImage:require("../assets/images/SplashLayoutImage.png"),
    SplashSmallImage:require("../assets/images/SplashSmallImage.png"),



    prayerLogo:require("../assets/images/appimages/prayer.jpeg"),
    login_prayLogo:require("../assets/images/appimages/5571027.png"),
    background_one:require("../assets/images/appimages/bg-2.png"),
    forgetpass_logo: require("../assets/images/appimages/forgot-password.png"),
    corrent_img: require("../assets/images/appimages/correct.png"),
    invoice_img: require("../assets/images/appimages/invoice.png"),
    
    
    },

}

export default AllSourcePath;