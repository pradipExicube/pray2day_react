import { Dimensions } from "react-native";

const windowHeight=Dimensions.get('window').height;
  const windowWidth = Dimensions.get('window').width;

const colors = {
    primary:'#3964AE',
    secondary:'#eb9800',
    tertiary:'#3A3A3C ',
    textPrimaryColor:'#3A3A3C',
    black:'#000000',
    grey:'#797979',
    white:'#FFFFFF',
    white:'#FFFFFF',
    darkgrey:'#21283E',
    lightgrey:'#F9F9F9',
    red:'#EF3722',
    blue:'#392BA4',
    textGrey:'#888FA8'
  };
  
  
  // Light theme colors
  const lightColors = {
    background: '#fff',
  };
  
  // Dark theme colors
  const darkColors = {
    // background: '#121212',
    background: '#fff',
  };
  
  
  const sizes = {
    s10: 10,
    s12: 12,
    s13: 13,
    s14: 14,
    s15: 15,
    s16: 16,
    s18: 18,
    s20: 20,
    s22: 22,
    s24: 24,
    s32:32,
    sm:9
  };

  
  
  const FontFamily = {
    light: 'Oswald-Light',
    normal: 'Oswald-Regular',
    medium: 'Oswald-Medium',
    bold: 'Oswald-Bold',
    semiBold:'Oswald-SemiBold',

    PlusJakartaSansNormal: 'PlusJakartaSans-Regular',
    PlusJakartaSansMedium: 'PlusJakartaSans-Medium',
    PlusJakartaSansBold: 'PlusJakartaSans-Bold',
    PlusJakartaSansSemiBold:'PlusJakartaSans-SemiBold',

  };
  
  
  const Theme = { colors, sizes, FontFamily, lightColors, darkColors,windowHeight,windowWidth };
  
  export default Theme;