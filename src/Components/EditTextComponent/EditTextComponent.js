import { View, Text, TextInput, TextInputComponent, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import Theme from '../../Constants/Theme'
import Icon from 'react-native-vector-icons/Ionicons'


const EditTextComponent = (props) => {
  
  return (
    <View style={[{borderRadius:4,flexDirection:'row',alignItems:'center',paddingHorizontal:10,backgroundColor:Theme.colors.inputFieldColor,height:45},props.style]}>
   
    {/* <View style={{flex:1,justifyContent:'center'}}> */}
      <TextInput 
      
      style={[{color:Theme.colors.black,flex:1,textAlign:'left',marginLeft:4,fontSize:14},props.textInputStyle]}
      editable={props.editable===null?true:props.editable} 
      selectTextOnFocus={props.selectTextOnFocus===null?true:props.selectTextOnFocus}
        secureTextEntry={props.secureTextEntry}
        placeholder={props.placeholder}
        placeholderStyle={props.placeholderStyle}
        onChangeText={props.onChangeText}
        keyboardType={props.keyboardType}
        value={props.value}
        placeholderTextColor={props.placeholderTextColor?props.placeholderTextColor:Theme.colors.inputFieldPlaceHolderColor}
        multiline={props.multiline || false}
        error={props.error}
        maxLength={props.maxLength}
        autoCapitalize={props.autoCapitalize?props.autoCapitalize:null}
        contextMenuHidden={props.contextMenuHidden?props.contextMenuHidden:false}
        ref={props.ref}
        autoFocus = {props.autoFocus}
        onKeyPress={props.onKeyPress}
        numberOfLines={props.numberOfLines}
      />
      {props.iconName ? 
      <View>
        <Icon onPress={props.iconPress} name={props.iconName} size={20} style={{paddingHorizontal:10}} />
      </View>
      :null }
    {/* </View> */}
   
    </View>
  )
}

export default EditTextComponent