import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import Theme from '../../Constants/Theme'
import Icon from 'react-native-vector-icons/Ionicons'

const CustomButton = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={[{backgroundColor:Theme.colors.secondary,borderRadius:6,alignItems:'center',justifyContent:'center',height:45},props.style]}>
        <View style={{flexDirection:'row',justifyContent:'center'}}>
          <Text style={[{color:Theme.colors.white,fontSize:Theme.sizes.s14,textAlign:'center',textTransform:'none'},props.buttonTextStyle]}>{props.buttonText} </Text>
          { props.customIconName ? 
          <Icon name={props.customIconName} color="#fff" size={16} style={props.customIconStyle ? props.customIconStyle : null} /> 
          : null }
        </View>
    </TouchableOpacity>
  )
}

export default CustomButton