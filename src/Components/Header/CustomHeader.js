import {
	View,
	Text,
	TextInput,
	TextInputComponent,
	TouchableOpacity,
	StyleSheet,
	Image,
} from 'react-native';
import React, { useState } from 'react';
import Theme from '../../Constants/Theme';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import BackIcon from '../../assets/icons/BackIcon';
import MenuIcon from '../../assets/icons/MenuIcon';
import AllSourcePath from '../../Constants/PathConfig';
import CrossIcon from '../../assets/icons/CrossIcon';
// import { useNavigation } from '@react-navigation/native';


const CustomHeader = props => {


	return (
		<View style={[{
			width: '100%',paddingHorizontal:'8%', alignItems: 'center', flexDirection: 'row', height: 60,
			alignSelf: "center",
			marginTop: Platform.OS === 'ios' ? 0 : 30,
			// borderBottomWidth:1,borderBottomColor:'#F1F1F1'
		}, props.style]}>
			

			{props.hideLeftComponentView ? null : <View style={[{ width: 0 }, props.leftComponentViewStyle]}>
				{props.LeftComponent ? props.LeftComponent : <BackIcon menuIconColor={props.IconColor} onPress={props.leftComponentOnPress} style={{ alignSelf: 'center', padding: 10 }} />}
			</View>}

			{props.hideRightComponentView ? null : <View style={[{ width: 0 }, props.rightComponentViewStyle]}>
				{props.rightComponent ? props.rightComponent : <MenuIcon menuIconColor={props.IconColor} onPress={props.rightComponentOnPress} style={{ alignSelf: 'center', padding: 10 }} />}
			</View>}


			{props.hideLeftComponentextratView ? null : <View style={[{ paddingLeft:25 }, props.LeftComponentextratStyle]}>
				{props.LeftComponentextrat ? props.LeftComponentextrat :null

					// <Image style={{ width: 98, height: 32 }} source={AllSourcePath.LOCAL_IMAGES.SplashSmallImage} />
					}
			</View>}


			{props.hideMiddleComponentView ? null : <View style={[{ width: "90%" }, props.middleComponentViewStyle]}>
				{props.middleComponent ? props.middleComponent : <Text style={[{ textAlign: 'center' }, props.middleTextStyle]}>{props.middleText}</Text>}
			</View>}



			{/* {props.hideRightComponentView ? null : <View style={[{ width: "10%" }, props.rightComponentViewStyle]}>
				{props.rightComponent ? props.rightComponent : <MenuIcon onPress={props.rightComponentOnPress} style={{ alignSelf: 'center', padding: 15 }} />}
			</View>} */}



		</View>
	);
};

const styles = StyleSheet.create({
	lefticonStyle: {
		// marginStart:'3%',
		marginRight: 10
		// padding:5
	},
	leftHeadingStyle: {
		color: Theme.colors.white,
		fontFamily: Theme.FontFamily.medium,
		fontSize: 15,
		textAlign: 'center',
		marginLeft: 69
	}
})

export default CustomHeader;

