import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Lottie from 'lottie-react-native'
import Theme from '../../Constants/Theme'

const NoDataFoundComponent = (props) => {
  
  return (
    <View style={[{alignItems:'center',alignSelf:'center',justifyContent:'center'},props.style]}>

         <Lottie  style={[{width:"50%",aspectRatio:1,alignSelf:'center'},props.lottieStyle]} source={require('../../assets/icons/no_data_found.json')} autoPlay loop />

         <Text style={[styles.noDataTextStyle,props.noDataTextStyle]}>{props.noDataText?props.noDataText:"No Data Found"}</Text>

    </View>
  )
}

export default NoDataFoundComponent

const styles = StyleSheet.create({
    noDataTextStyle:{
        fontFamily:Theme.FontFamily.medium,
        fontSize:14,
        color:Theme.colors.grey
    }
})