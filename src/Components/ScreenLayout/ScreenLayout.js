import {
	View,
	Text,
	TextInput,
	TextInputComponent,
	TouchableOpacity,
	StatusBar,
	SafeAreaView,
	ScrollView,
	Dimensions,
	KeyboardAvoidingView,
	Platform,
	Keyboard,
	ActivityIndicator,
} from 'react-native';
import React, { useEffect, useState } from 'react';

import Theme from '../../Constants/Theme';
import CustomHeader from '../Header/CustomHeader';
// import Lottie from 'lottie-react-native';
import { useDispatch } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';


const ScreenLayout = props => {
	const dispatch = useDispatch();

	// useEffect(()=>{
	// 	const keyboardDidShowListener = Keyboard.addListener(
	// 	  'keyboardDidShow',
	// 	  (keyBoardData) => {
	// 		console.log(keyBoardData)
	// 	  }
	// 	);
	// 	const keyboardDidHideListener = Keyboard.addListener(
	// 	  'keyboardDidHide',
	// 	  (closeData) => {
	// 		console.log(closeData)
	// 	  }
	// 	);

	// 	return () => {
	// 	  keyboardDidHideListener.remove();
	// 	  keyboardDidShowListener.remove();
	// 	}; 
	//   },[])

	return (
		<>

			<SafeAreaView
				style={[{
					backgroundColor: props.layoutBackground ? props.layoutBackground : '#E6E6E6',
					flex: 1
				}, props.style]}

			>
				{/* <LinearGradient colors={['#E6E6E6', '#F4F4F4',]}>  */}
				<StatusBar
					backgroundColor={props.customStatusbarColor ? props.customStatusbarColor : '#E6E6E6'}
					animated={true}
					barStyle={'dark-content'}
					translucent={true}
				/>
				{!props.hideCustomHeader ?

					<View style={{ width: '100%' }}>
						<CustomHeader
							IconColor={props.IconColor}
							style={props.headerStyle}
							hideLeftComponentView={props.hideLeftComponentView}
							leftComponentViewStyle={props.leftComponentViewStyle}
							LeftComponent={props.LeftComponent}
							leftComponentOnPress={props.leftComponentOnPress}

							hideMiddleComponentView={props.hideMiddleComponentView}
							middleComponentViewStyle={props.middleComponentViewStyle}
							middleComponent={props.middleComponent}
							middleTextStyle={props.middleTextStyle}
							middleText={props.middleText}
							
							hideRightComponentView={props.hideRightComponentView}
							rightComponentViewStyle={props.rightComponentViewStyle}
							rightComponent={props.rightComponent}
							rightComponentOnPress={props.rightComponentOnPress}
							LeftComponentextrat={props.LeftComponentextrat}
						/>

					</View> : null
				}
				<KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === "ios" ? "padding" : undefined} >
					{props.isScrollable ?
						<ScrollView
							nestedScrollEnabled={props.nestedScrollEnabled}
							showsVerticalScrollIndicator={false}
							style={[{
								flex: 1,
								backgroundColor: props.isWhite ? '#FFFFFF' : '#F4F4F4',
							}, props.viewStyle]} refreshControl={props.refreshControl}>
							{props.children}
						</ScrollView> : <View style={[{
							flex: 1,
							backgroundColor: props.isWhite ? '#FFFFFF' : '#F8F9FF'
						}, props.viewStyle]}>
							{props.children}
						</View>
					}
				</KeyboardAvoidingView>

				{/* </LinearGradient> */}
			</SafeAreaView>
			{props.showLoading ?
				<View style={{ position: 'absolute', height: '100%', width: '100%', bottom: 0, backgroundColor: 'black', opacity: 0.4 }}>
					<View style={{ position: 'absolute', height: '100%', width: '100%', top: 0 }}>
						<View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
							<View style={{ justifyContent: 'center', flexDirection: 'row' }}>
								{/* <Lottie style={{width:200,aspectRatio:1,backgroundColor:'white' }} source={require('../../assets/icons/loading.json')} autoPlay loop />
											<View style={{width:100,aspectRatio:1,backgroundColor:'red',borderRadius:100}}>
											<Lottie source={require('../../assets/icons/loading.json')} autoPlay loop />
												
											</View> */}
							</View>
						</View>
					</View>
				</View> : null}

			{props.showLoading ?
				<View style={{ position: 'absolute', height: '100%', width: '100%', bottom: 0 }}>
					<View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
						<View style={{
							justifyContent: 'center', flexDirection: 'column', alignItems: 'center', backgroundColor: 'white', width: 70, height: 70, alignSelf: 'center', borderRadius: 10,
							shadowColor: '#000',
							shadowOffset: { width: 0, height: 0 },
							shadowOpacity: 0.2,
							shadowRadius: 6,
							elevation: 6,
							backgroundColor: "white",
							// borderRadius:70

						}}>
							{/* <Lottie style={{width:90,aspectRatio:1 }} source={require('../../assets/icons/loading.json')} autoPlay loop /> */}
							<ActivityIndicator size={'large'} color={'#00AD70'} />
							{/* <Lottie style={{width:90,aspectRatio:1 }} source={require('../../assets/icons/new_loading.json')} autoPlay loop /> */}
						</View>
					</View>
				</View> : null}
			{
				// console.log('frawer',props.onLeftIconPress)
			}

		</>
	);
};

export default ScreenLayout;

/*

	<CustomHeader
		showLeftIcon={props.showLeftIcon} 
		LeftIconComponent={props.LeftIconComponent}
		RightIconComponent={props.RightIconComponent} 
		RightIconStyle={props.RightIconStyle}
		LeftIconStyle={props.LeftIconStyle} 
		leftIconPress={props.leftIconPress}
		valueCount={props.valueCount} 
		hideBottomBorder={props.hideBottomBorder}
		HeaderColor={props.HeaderColor} 
		HeaderStyle={props.HeaderStyle}
		showHeaderTitle={props.showHeaderTitle} 
		HeaderTitleValue={props.HeaderTitleValue}
		HeaderSubTitleValue={props.HeaderSubTitleValue} 
		showRightIcon={props.showRightIcon} 
		RightIconPress={props.RightIconPress}

/>
*/
