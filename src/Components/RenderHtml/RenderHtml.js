import React from 'react';
import {Dimensions, StyleSheet, useWindowDimensions} from 'react-native';
// import RenderHTML from 'react-native-render-html';
import HTMLView from 'react-native-render-html';
import Theme from '../../Constants/Theme';



const WebDisplay = React.memo(function WebDisplay({html}) {
  const {width: contentWidth} = useWindowDimensions();
  return (
    // <RenderHTML
    // systemFonts={['Arial']}
    //   contentWidth={contentWidth}
    //   source={{html}}
    //   baseStyle={{color: Theme.colors.white, lineHeight: 22}}
    //   ignoredDomTags={['font']}
    // />
    <HTMLView
    source={{html}}
    contentWidth={contentWidth}
    // stylesheet={styles}
  />
  );
});

export default WebDisplay;
const styles = StyleSheet.create({
  a: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  p: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  span: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  h1: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  h2: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  h3: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  ul: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  li: {
    fontWeight: '400',
    color: Theme.colors.black,
    lineHeight: 22 
  },
  img: {
    height:500,
    width:500
  }

});
