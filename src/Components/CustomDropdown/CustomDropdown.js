import { View, Text, TouchableOpacity, FlatList, Pressable, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import Theme from '../../Constants/Theme'
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import AllSourcePath from '../../Constants/PathConfig';

const maxHeight = 200;

const CustomDropdown = (props) => {

	const [showListSection, setShowListSection] = useState(false)
	useEffect(() =>{
		if(props.value.length ==0){
			setShowListSection(false)
		}
	})


	const flatlist_countryList = (data) =>{
    // console.log("data : ",data?.item);
    return(
      <TouchableOpacity 
				onPress={()=>{
					props.onPress(data.item);
					setShowListSection(!showListSection);
				}}
				style={
					[{padding:10,flexDirection:'row',justifyContent:'space-between',borderBottomWidth:0.5,borderColor:'#E7E8E9'},props.ItemStyle]
				}>
        {/* <View style={{flex:1,justifyContent:'center'}}><Text>{data?.item?.name}</Text></View> */}
				<View style={{flex:1,flexDirection:'row'}}>
					{data?.item?.img_url ? 
						<View style={{justifyContent:'center',marginRight:10}}>
							<Image 
								source={
									data?.item?.img_url ? 
										{ uri: data?.item?.image } 
									:
										AllSourcePath.LOCAL_IMAGES.prayerLogo
								}
								style={{
										height: 24,
										width: 30,
								}}
								resizeMode='cover'
							/>
						</View>
						:null }
						<View style={{justifyContent:'center'}}><Text>{data?.item?.name}</Text></View>

				</View>

        <View style={{justifyContent:'center',paddingHorizontal:10}}><Icon name="chevron-forward-outline" /></View>
      </TouchableOpacity>
    )
  }

	return (
		<View>
			<Pressable 
			onPress={()=>{setShowListSection(!showListSection)}}
			style={{ height: 45, borderWidth: 1, borderRadius: 6, borderColor: '#E7E8E9', flexDirection: 'row', justifyContent: 'space-between', borderBottomLeftRadius: props.countryList?.length >0 ? 0 : 6, borderBottomRightRadius: props.countryList?.length >0 ? 0 : 6 }}>
				<View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
					<Text>{props.title}</Text>
				</View>
				<View style={{ justifyContent: 'center', paddingHorizontal: 15 }}>
					<Icon name="chevron-down" size={16} />
				</View>
			</Pressable>
			{props.value?.length >0 && showListSection ? 
				<Animatable.View animation={"fadeIn"} style={[{ maxHeight: maxHeight, borderWidth: 1, borderColor: '#E7E8E9', padding: 5, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 0 },props.ContainerStyle]}>
					<FlatList
						showsVerticalScrollIndicator={true}
						data={props.value}
						renderItem={flatlist_countryList}
					/>
				</Animatable.View>
				:null
			}
		</View>
	)
}

export default CustomDropdown