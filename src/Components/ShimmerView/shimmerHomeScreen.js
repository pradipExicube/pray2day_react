import { FlatList, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const shimmerHomeScreen = () => {
  return (
    <View style={{flex:1}}>
        <FlatList
    style={{ alignSelf: 'center', width: '94%', marginBottom: '5%' }}
    renderItem={() => {
      return (
        <SkeletonPlaceholder borderRadius={4}>
          <SkeletonPlaceholder.Item
            justifyContent="center"
            height={55}
            width={'100%'}
            marginVertical={5}
            flexDirection="row"
            marginTop={'3%'}
            borderRadius={5}
            paddingHorizontal={8}
          >
            <SkeletonPlaceholder.Item
              style={{ marginVertical: 5 }}
              flexDirection="row"
              alignItems="center"
              width={"100%"}>
              <SkeletonPlaceholder.Item
                width={60}
                height={60}
              />
              <SkeletonPlaceholder.Item borderRadius={4} width={"80%"} marginLeft={20}>
                <SkeletonPlaceholder.Item
                  width={"90%"} height={60}
                />
              </SkeletonPlaceholder.Item>
            </SkeletonPlaceholder.Item>
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      )
    }}
    data={[1, 1]}
  />
  </View>

  )
}

export default shimmerHomeScreen

const styles = StyleSheet.create({})