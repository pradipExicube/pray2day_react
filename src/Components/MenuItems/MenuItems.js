import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import HelperFunctions from '../../Constants/HelperFunctions'
import Rightarrow from '../../assets/icons/Rightarrow';
import Theme from '../../Constants/Theme';

const { width, height } = Dimensions.get('screen');

const MenuItems = ({item}) => {
  return (
    <TouchableOpacity onPress={item?.onPress?item.onPress:()=>HelperFunctions.showToastMsg("Page Under development")} style={styles.mainbox}>
      <View style={styles.insidecontent}>
        <View style={{
          flexDirection: "row",
          justifyContent: "space-between"
        }}>
          <View style={{
            marginRight: "6%"
          }}>
            {item?.svg}
          </View>
          <Text style={styles.titletext}>{item.title}</Text>
        </View>
        <Rightarrow />
      </View>


    </TouchableOpacity>
  )
}

export default MenuItems

const styles = StyleSheet.create({
    mainbox:{
        height: 50,
        width: width - 40,
        backgroundColor: "#FFFFFF",
        alignSelf: "center",
        borderRadius: 5,
        marginTop: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        justifyContent: "center",
        elevation: 5, 
      },
      insidecontent:{
        flexDirection: "row",
      justifyContent: "space-between",
      marginHorizontal: 10},
      titletext:{
        alignSelf: "center",
        color: "#0C0000",
        fontSize: 16,
        fontFamily: Theme.FontFamily.medium,
      }
})