import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import Theme from '../../Constants/Theme'
import Icon from 'react-native-vector-icons/Ionicons'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const CustomScleton = (props) => {

    const showScletonView = (props) => {
        return (
            <View style={{ marginTop: 10 }}>
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '90%', height: 40, borderRadius: 5, marginLeft: 20 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10}} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
    
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '90%', height: 40, borderRadius: 5, marginLeft: 20 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10}} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
    
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '90%', height: 40, borderRadius: 5, marginLeft: 20 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10}} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
    
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '90%', height: 40, borderRadius: 5, marginLeft: 20 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              <View style={{ marginTop: 10}} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
    
              <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '90%', height: 40, borderRadius: 5, marginLeft: 20 }} />
                  </View>
                </SkeletonPlaceholder>
              </View>
              {/* <View style={{ marginTop: 10 }} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View> */}
              {/* <View style={{ marginTop: 10}} >
                <SkeletonPlaceholder borderRadius={4}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                    <View style={{ width: '48%', height: 40, borderRadius: 5 }} />
                  </View>
                </SkeletonPlaceholder>
              </View> */}
              
    
            </View>
            
        )
      }


  return (
    showScletonView()
  )
}

export default CustomScleton