import { createSlice } from '@reduxjs/toolkit';
import { Alert } from 'react-native';

const initialState = {
    globalData:{},
    access_token:"",
    user_details:{},
    subscriptionDetails:{},
    aboutusContent:null,
    privacyyContent:null,
    termsContent:null,
    store_countryList:[],

    api_subscriptionList:[],
    store_paymentHistory:[],
    store_chaptarDetails:null
};

export const commonReducer = createSlice({
    name: 'commonData',
    initialState,
    reducers: {
        setcommonglobalData:(state, action) => {
            state.globalData = action.payload
        },
        store_setAccessToken:(state, action) => {
            state.access_token = action.payload
        },
        store_setUserdetails:(state, action) => {
            state.user_details = action.payload
        },
        set_subscriptionDetails:(state, action) => {
            state.subscriptionDetails = action.payload
        },

        set_aboutusContent:(state, action) => {
            state.aboutusContent = action.payload
        },
        set_privacyyContent:(state, action) => {
            state.privacyyContent = action.payload
        },
        set_termsContent:(state, action) => {
            state.termsContent = action.payload
        },
        set_Contry:(state, action) => {
            state.store_countryList = action.payload
        },
        
        

        set_subscriptionList:(state, action) => {
            state.api_subscriptionList = action.payload
        },
        set_paymentHistory:(state, action) => {
            // console.log("======= payment store =====>", action.payload)
            state.store_paymentHistory = action.payload
        },
        set_store_chaptarDetails:(state, action) => {
            // console.log("======= payment store =====>", action.payload)
            state.store_chaptarDetails = action.payload
        },

        
    },
});


export const {
    setcommonglobalData,
    store_setAccessToken,
    store_setUserdetails,
    set_subscriptionDetails,
    set_aboutusContent,
    set_Contry,

    set_subscriptionList,
    set_paymentHistory,
    set_privacyyContent,
    set_termsContent,
    set_store_chaptarDetails
} = commonReducer.actions;

export default commonReducer.reducer;
