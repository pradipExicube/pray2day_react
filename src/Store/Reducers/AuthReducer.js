import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    userDetails: {},
    token: ''
    
};

export const authReducer = createSlice({
    name: 'authData',
    initialState,
    reducers: {
        setToken: (state, action) => {
            state.token = action.payload;
        },

        setUserDetails: (state, action) => {
            state.userDetails = action.payload
        },
        setExtraUserDetails:(state,action)=>{
            state.userDetails = {...state.userDetails, ...action.payload}
        },
        resetAuthData: (state) => {
            state.userDetails = {},
            state.token = ""
        },

    },
});

// Action creators are generated for each case reducer function
export const {
    setToken,
    setUserDetails,
    setExtraUserDetails,
    resetAuthData
} = authReducer.actions;

export default authReducer.reducer;
