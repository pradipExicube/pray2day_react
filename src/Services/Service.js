import axios from "axios";
import { useSelector } from "react-redux";
import AllSourcePath from "../Constants/PathConfig";


/* ==== Post Api Without Token ==== */
const api_postWithoutToken = (
  endpoint, 
  data, 
  ContentType="application/json",
  baseUrl=AllSourcePath.API_BASE_URL_DEV
) => {  
  return new Promise((resolve, reject) => {
    axios.post(baseUrl + endpoint, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": ContentType,
        // 'Authorization': token
      },
    })
      .then((response) => {
        // console.log(endpoint, "api response data=====>")
        // console.log(response.data)
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response!=undefined || error.response!=null) {
          if(error.response.data!=undefined || error.response.data!=null){
            reject(error.response.data);
          }
          else{
            error['msg']=error?.message?error?.message:'Unknown Error'
            reject(error);
          }
        } else {
          error['msg']=error?.message?error?.message:'Unknown Error'
          reject(error);
        }
        
        
      });
  });
};

/* ==== Get Api Without Token ==== */
const api_getWithoutToken = (
  endpoint, 
  data, 
  ContentType="application/json",
  baseUrl=AllSourcePath.API_BASE_URL_DEV
) => {  
  return new Promise((resolve, reject) => {
    axios.get(baseUrl + endpoint, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": ContentType,
        // 'Authorization': token
      },
    })
      .then((response) => {
        // console.log(endpoint, "GET api response data=====>")
        // console.log(response.data)
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response!=undefined || error.response!=null) {
          if(error.response.data!=undefined || error.response.data!=null){
            reject(error.response.data);
          }
          else{
            error['msg']=error?.message?error?.message:'Unknown Error'
            reject(error);
          }
        } else {
          error['msg']=error?.message?error?.message:'Unknown Error'
          reject(error);
        }
        
        
      });
  });
};


/* ==== Post Api with Token ==== */
const api_postWithToken = (
  endpoint, 
  data, 
  userToken,
  ContentType="application/json",
  baseUrl=AllSourcePath.API_BASE_URL_DEV
) => {  
  return new Promise((resolve, reject) => {
    axios.post(baseUrl + endpoint, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": ContentType,
        // 'Authorization': token
        'Authorization': 'Bearer ' + userToken,
        // "Accept":"application/json"
      },
    })
      .then((response) => {
        console.log(endpoint, "api response data=====>")
        console.log(response.data)
        resolve(response.data);
      })
      .catch((error) => {
        console.log("error : ==>", endpoint)
        if (error.response!=undefined || error.response!=null) {
          if(error.response.data!=undefined || error.response.data!=null){
            reject(error.response.data);
          }
          else{
            error['msg']=error?.message?error?.message:'Unknown Error'
            reject(error);
          }
        } else {
          error['msg']=error?.message?error?.message:'Unknown Error'
          reject(error);
        }
        
        
      });
  });
};


// const api_uploadImage = (
//   endpoint, 
//   data, 
//   userToken,
//   ContentType="application/json",
//   baseUrl=AllSourcePath.API_BASE_URL_DEV
// ) => {  
//   return new Promise((resolve, reject) => {
//     console.log(data)
//     const form = new FormData();
//     form.append('type',  data?.type);
//     form.append('profile_picture', {
//       uri: data?.assets[0].uri,
//       type: data?.assets[0].type,
//       name: data?.assets[0].fileName,
//     });
//     // bodyFormData.append('type', data?.type);

//     axios.post(baseUrl + endpoint, form, {
//       headers: {
//         Accept: "application/json",
//         "Content-Type": ContentType,
//         // 'Authorization': token
//         'Authorization': 'Bearer ' + userToken,
//         "Accept":"application/json"
//       },
//     })
//       .then((response) => {
//         console.log(endpoint, "api response data=====>")
//         console.log(response.data)
//         resolve(response.data);
//       })
//       .catch((error) => {
//         if (error.response!=undefined || error.response!=null) {
//           if(error.response.data!=undefined || error.response.data!=null){
//             reject(error.response.data);
//           }
//           else{
//             error['msg']=error?.message?error?.message:'Unknown Error'
//             reject(error);
//           }
//         } else {
//           error['msg']=error?.message?error?.message:'Unknown Error'
//           reject(error);
//         }
        
        
//       });
//   });
// };


const postApi = (
  endpoint, 
  data, 
  token,
  ContentType="application/json",
  baseUrl=AllSourcePath.API_BASE_URL_DEV
) => {  
  return new Promise((resolve, reject) => {
    axios.post(baseUrl + endpoint, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": ContentType,
        'Authorization': token
      },
    })
      .then((response) => {
        console.log(endpoint, "api response data=====>")
        console.log(response.data)
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response!=undefined || error.response!=null) {
          if(error.response.data!=undefined || error.response.data!=null){
            reject(error.response.data);
          }
          else{
            error['msg']=error?.message?error?.message:'Unknown Error'
            reject(error);
          }
        } else {
          error['msg']=error?.message?error?.message:'Unknown Error'
          reject(error);
        }
        
        
      });
  });
};


const getApi = (endpoint, data, token,ContentType="application/json",baseUrl=AllSourcePath.API_BASE_URL_DEV) => {
  console.log(endpoint, "api endpoint and params and token =====>")
  console.log("Endpoint", endpoint);
  console.log("params", data)
  console.log("Token", token)
  console.log("baseUrl", baseUrl)
  console.log("==============================");

  
  return new Promise((resolve, reject) => {
    axios.get(baseUrl+endpoint,{
      params:data,
      headers: {
        Accept: "application/json",
        "Content-Type": ContentType,
        'x-access-token': token
      }
    })
      .then((response) => {
        console.log(endpoint, "api response data=====>")
        console.log(response.data)
        resolve(response.data);
      })
      .catch((error) => {
        console.log(error)
        
        if (error.response!=undefined || error.response!=null) {
          if(error.response.data!=undefined || error.response.data!=null){
            reject(error.response.data);
          }
          else{
            error['msg']=error?.message?error?.message:'Unknown Error'
            reject(error);
          }
        } else {
          error['msg']=error?.message?error?.message:'Unknown Error'
          reject(error);
        }
        
        
      });
  });
};



const demoStripeAPi = (value) => {

  return new Promise((resolve, reject) => {
    axios.get("https://pray2day.ca/api/debug/stripe/initiate?amount="+ value,{
      // params:data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        // 'x-access-token': token
      }
    })
      .then((response) => {
        // console.log(endpoint, "api response data=====>")
        console.log(response?.data)
        resolve(response?.data);
      })
      .catch((error) => {
        console.log("1 :", error)
        
        if (error.response!=undefined || error.response!=null) {
          if(error.response.data!=undefined || error.response.data!=null){
            reject(error.response.data);
          }
          else{
            error['msg']=error?.message?error?.message:'Unknown Error'
            reject(error);
          }
        } else {
          error['msg']=error?.message?error?.message:'Unknown Error'
          reject(error);
        }
        
        
      });
  });
};




export {postApi,getApi,demoStripeAPi,
  api_postWithoutToken,
  api_getWithoutToken,
  api_postWithToken,
  // api_uploadImage

};