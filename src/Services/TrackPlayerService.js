import TrackPlayer from 'react-native-track-player';

const onRegisterPlayback = async () => {

	TrackPlayer.addEventListener('remote-play', () => TrackPlayer.play());
	TrackPlayer.addEventListener('remote-pause', () => TrackPlayer.pause());
	TrackPlayer.addEventListener('remote-stop', () => TrackPlayer.destroy())
}

const setUpPlayer = async () => {
	try {
		await TrackPlayer.setupPlayer();
		return true;

	} catch (error) {
		console.log("setUpPlayer error  : ", JSON.stringify(error))
		return false;
	}
}

const addTrack = async (songUrl) => {
	try {
		await TrackPlayer.add({
			url: songUrl
		})

	} catch (error) {
		console.log("Add Track error  : ", JSON.stringify(error))
	}
}

const playTrack = async () => {
	TrackPlayer.play()
	setisPlayTrack(true)
	// return true;
}
const pauseTrack = async () => {
	TrackPlayer.pause()
	// setisPlayTrack(false)
	// return false;
}

const stopTrack = async () => {
	TrackPlayer.stop()
	// setisPlayTrack(false)
	// return false;
}
const resetTrack = async () => {
	TrackPlayer.reset()
	// setisPlayTrack(false)
	// return false;
}





export { onRegisterPlayback,setUpPlayer,addTrack, playTrack,pauseTrack,stopTrack,resetTrack };