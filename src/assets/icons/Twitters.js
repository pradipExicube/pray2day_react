import * as React from "react";
import Svg, { G, Rect, Path } from "react-native-svg";
const Twitter = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_31443" data-name="Group 31443" transform="translate(997 108)">
      <Rect
        id="Rectangle_2661"
        data-name="Rectangle 2661"
        width={24}
        height={24}
        transform="translate(-997 -108)"
        fill="none"
      />
      <G id="Layer_2" transform="translate(-997.5 -108.5)">
        <Path
          id="Path_139"
          data-name="Path 139"
          d="M12.5,2.5C.1,2.863-1.162,20.261,10.93,22.5h3.141C26.166,20.259,24.9,2.862,12.5,2.5Z"
          fill="#1877f2"
        />
        <Path
          id="Path_140"
          data-name="Path 140"
          d="M35.436,29.135H37.5l.393-2.564H35.436V24.908a1.281,1.281,0,0,1,1.444-1.385H38V21.34c-2.393-.431-5.268-.3-5.33,3.277v1.954h-2.25v2.564h2.25v6.2h2.769Z"
          transform="translate(-21.514 -12.832)"
          fill="#fff"
        />
      </G>
    </G>
  </Svg>
);
export default Twitter;