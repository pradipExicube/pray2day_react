import * as React from "react";
import Svg, { G, Rect, Circle, Path } from "react-native-svg";
const Linkdin = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_31446" data-name="Group 31446" transform="translate(997 108)">
      <Rect
        id="Rectangle_2661"
        data-name="Rectangle 2661"
        width={24}
        height={24}
        transform="translate(-997 -108)"
        fill="none"
      />
      <Circle
        id="Ellipse_7"
        data-name="Ellipse 7"
        cx={10}
        cy={10}
        r={10}
        transform="translate(-995 -106)"
        fill="#0076b9"
      />
      <G
        id="Group_31445"
        data-name="Group 31445"
        transform="translate(-989.268 -100.268)"
      >
        <Path
          id="Path_141"
          data-name="Path 141"
          d="M0,5H1.909v5.869H0Zm7.109.069L7.048,5.05,6.97,5.035A1.711,1.711,0,0,0,6.628,5,2.652,2.652,0,0,0,4.577,6.122V5H2.668v5.869H4.577v-3.2s1.443-2.009,2.051-.534v3.735H8.536V6.908a1.9,1.9,0,0,0-1.428-1.84Z"
          transform="translate(0 -2.332)"
          fill="#fff"
        />
        <Circle
          id="Ellipse_8"
          data-name="Ellipse 8"
          cx={0.934}
          cy={0.934}
          r={0.934}
          fill="#fff"
        />
      </G>
    </G>
  </Svg>
);
export default Linkdin;