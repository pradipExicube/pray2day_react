import * as React from "react";
import Svg, { G, Rect, Circle, Path } from "react-native-svg";
const Twitter= (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_31444" data-name="Group 31444" transform="translate(997 108)">
      <Rect
        id="Rectangle_2661"
        data-name="Rectangle 2661"
        width={24}
        height={24}
        transform="translate(-997 -108)"
        fill="none"
      />
      <Circle
        id="Ellipse_7"
        data-name="Ellipse 7"
        cx={10}
        cy={10}
        r={10}
        transform="translate(-995 -106)"
      />
      <Path
        id="Path_143"
        data-name="Path 143"
        d="M281.526,167.31l4.168,5.573-4.194,4.531h.944l3.672-3.967,2.967,3.967H292.3l-4.4-5.886,3.9-4.218h-.944l-3.382,3.654-2.732-3.654Zm1.388.7h1.476l6.517,8.713h-1.476Z"
        transform="translate(-1271.898 -268.362)"
        fill="#fff"
      />
    </G>
  </Svg>
);
export default Twitter;