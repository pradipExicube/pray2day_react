import * as React from "react";
import Svg, { SvgProps, G, Rect, Path } from "react-native-svg";
const BackIcon = (props: SvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_230" data-name="Group 230" transform="translate(-0.137)">
      <Rect
        id="Rectangle_74"
        data-name="Rectangle 74"
        width={24}
        height={24}
        transform="translate(0.137)"
        fill="none"
      />
      <G
        id="Group_6401"
        data-name="Group 6401"
        transform="translate(-14.614 -13.966)"
      >
        <Path
          id="Path_56"
          data-name="Path 56"
          d="M27.952,33.832l-.854-.854-2.037-2.037-2.475-2.475-2.127-2.127c-.345-.345-.685-.694-1.036-1.036l-.015-.015v1.356l.854-.854L22.3,23.753l2.475-2.475L26.9,19.152c.345-.345.694-.687,1.036-1.036l.015-.015a.963.963,0,0,0,0-1.358.966.966,0,0,0-.679-.28,1.007,1.007,0,0,0-.679.28l-.854.854L23.7,19.633l-2.475,2.475L19.1,24.235c-.345.345-.694.687-1.036,1.036l-.015.015a.974.974,0,0,0,0,1.356c.286.288.572.572.856.857l2.037,2.037,2.475,2.475,2.127,2.127c.345.345.687.694,1.036,1.036l.015.015a.963.963,0,0,0,1.358,0,.966.966,0,0,0,.28-.679,1.009,1.009,0,0,0-.282-.679Z"
          transform="translate(-0.504 0)"
          fill={props.menuIconColor ? props.menuIconColor : '#000'}
        />
        <Path
          id="Path_57"
          data-name="Path 57"
          d="M18.221,463.875H34.588c.221,0,.443,0,.664,0h.029a1,1,0,0,0,.679-.28.963.963,0,0,0,0-1.358,1,1,0,0,0-.679-.28H18.914c-.221,0-.443,0-.664,0h-.029a1,1,0,0,0-.679.28.963.963,0,0,0,0,1.358A1.011,1.011,0,0,0,18.221,463.875Z"
          transform="translate(0 -436.948)"
          fill={props.menuIconColor ? props.menuIconColor : '#000'}
        />
      </G>
    </G>
  </Svg>
);
export default BackIcon;
