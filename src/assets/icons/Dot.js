import * as React from "react";
import Svg, { G, Rect, Circle } from "react-native-svg";
const Dot = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Layer_2" data-name="Layer 2" opacity={0.54}>
      <G id="more-vertical">
        <Rect
          id="Rectangle_2672"
          data-name="Rectangle 2672"
          width={24}
          height={24}
          transform="translate(0 24) rotate(-90)"
          fill="#0c0000"
          opacity={0}
        />
        <Circle
          id="Ellipse_9"
          data-name="Ellipse 9"
          cx={2}
          cy={2}
          r={2}
          transform="translate(10 10)"
          fill="#0c0000"
        />
        <Circle
          id="Ellipse_10"
          data-name="Ellipse 10"
          cx={2}
          cy={2}
          r={2}
          transform="translate(10 3)"
          fill="#0c0000"
        />
        <Circle
          id="Ellipse_11"
          data-name="Ellipse 11"
          cx={2}
          cy={2}
          r={2}
          transform="translate(10 17)"
          fill="#0c0000"
        />
      </G>
    </G>
  </Svg>
);
export default Dot;
