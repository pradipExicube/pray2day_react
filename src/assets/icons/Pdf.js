import * as React from "react";
import Svg, { G, Path } from "react-native-svg";
const Pdf = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={26}
    height={32}
    viewBox="0 0 26 32"
    {...props}
  >
    <G
      id="Group_6428"
      data-name="Group 6428"
      transform="translate(-79.097 -274)"
    >
      <G
        id="_1add3ac4a7ef158fa675ddf73db811ac"
        data-name="1add3ac4a7ef158fa675ddf73db811ac"
        transform="translate(65.097 272)"
      >
        <Path
          id="Path_67"
          data-name="Path 67"
          d="M37.4,34H16.6A2.593,2.593,0,0,1,14,31.419V4.581A2.593,2.593,0,0,1,16.6,2H27.179a2.592,2.592,0,0,1,1.838.756L39.238,12.9A2.553,2.553,0,0,1,40,14.726V31.419A2.593,2.593,0,0,1,37.4,34ZM16.6,3.032a1.556,1.556,0,0,0-1.56,1.548V31.419a1.556,1.556,0,0,0,1.56,1.548H37.4a1.556,1.556,0,0,0,1.56-1.548V14.726a1.554,1.554,0,0,0-.457-1.095L28.282,3.486a1.556,1.556,0,0,0-1.1-.453Z"
          transform="translate(0 0)"
          fill="#fd4233"
        />
        <Path
          id="Path_68"
          data-name="Path 68"
          d="M49.023,51.147a.812.812,0,0,0-.24-.408,3.367,3.367,0,0,0-2.155-.5,18.009,18.009,0,0,0-2.591.188,6.457,6.457,0,0,1-1.2-.821,8.66,8.66,0,0,1-2.17-3.326c.031-.121.057-.227.084-.335a21.394,21.394,0,0,0,.381-3.9,1.121,1.121,0,0,0-.068-.279l-.044-.114a.932.932,0,0,0-.851-.645L39.911,41h-.005a.963.963,0,0,0-.982.609c-.313,1.146.01,2.862.6,5.081l-.151.361c-.42,1.017-.948,2.039-1.413,2.939l-.06.116c-.491.947-.935,1.752-1.34,2.434l-.415.217c-.031.015-.744.39-.911.488-1.418.839-2.358,1.788-2.515,2.542a.62.62,0,0,0,.24.692l.4.2a1.248,1.248,0,0,0,.548.129c1.011,0,2.183-1.244,3.8-4.031a38.176,38.176,0,0,1,5.853-1.373,10.184,10.184,0,0,0,4.265,1.337,2,2,0,0,0,.5-.054.871.871,0,0,0,.5-.335A1.768,1.768,0,0,0,49.023,51.147ZM33.688,56.053A6.95,6.95,0,0,1,35.68,53.7c.068-.054.235-.209.387-.354C34.939,55.121,34.184,55.828,33.688,56.053Zm6.388-14.537c.324,0,.509.808.525,1.566a3.19,3.19,0,0,1-.387,1.685,8.089,8.089,0,0,1-.274-2.1S39.927,41.516,40.076,41.516ZM38.169,51.875q.341-.6.7-1.27A23.64,23.64,0,0,0,40.1,47.947a8.813,8.813,0,0,0,2.034,2.5c.1.083.209.168.319.253A26.079,26.079,0,0,0,38.169,51.875Zm10.3-.09a1.52,1.52,0,0,1-.567.1,6.713,6.713,0,0,1-2.35-.7c.394-.028.757-.044,1.081-.044a4.2,4.2,0,0,1,1.35.145C48.561,51.426,48.568,51.723,48.466,51.785Z"
          transform="translate(-13.872 -28.936)"
          fill="#ff402f"
        />
      </G>
    </G>
  </Svg>
);
export default Pdf;
