import * as React from "react";
import Svg, { G, Rect, Path } from "react-native-svg";
const Religious = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G
      id="Group_6703"
      data-name="Group 6703"
      transform="translate(997 108)"
      opacity={0.54}
    >
      <Rect
        id="Rectangle_2661"
        data-name="Rectangle 2661"
        width={24}
        height={24}
        transform="translate(-997 -108)"
        fill="none"
      />
      <G
        id="Group_6792"
        data-name="Group 6792"
        transform="translate(-998.238 -109.03)"
      >
        <Path
          id="Path_136"
          data-name="Path 136"
          d="M4.964,19.759a3.264,3.264,0,0,0,3.45,3.029H20.689a.824.824,0,0,0,.646-1.337,2.4,2.4,0,0,1,0-3.384.817.817,0,0,0,.085-.145c.009-.019.023-.034.031-.054a.818.818,0,0,0,.056-.278c0-.012.007-.021.007-.033V3.824A.824.824,0,0,0,20.689,3H7.879A2.919,2.919,0,0,0,4.964,5.916Zm14.29,1.38H8.414a1.656,1.656,0,0,1-1.8-1.38,1.656,1.656,0,0,1,1.8-1.38h10.84A3.678,3.678,0,0,0,19.254,21.139ZM7.879,4.649H19.864V16.73H8.414a3.772,3.772,0,0,0-1.8.458V5.916A1.268,1.268,0,0,1,7.879,4.649Z"
          transform="translate(0 0)"
        />
        <Path
          id="Path_137"
          data-name="Path 137"
          d="M11.782,10.275h1.684V15.36a.824.824,0,0,0,1.649,0V10.275H16.8a.824.824,0,1,0,0-1.649H15.115V7.046a.824.824,0,1,0-1.649,0v1.58H11.783a.824.824,0,0,0,0,1.649Z"
          transform="translate(-1.052 -0.565)"
        />
      </G>
    </G>
  </Svg>
);
export default Religious;
