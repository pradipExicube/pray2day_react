import * as React from "react";
import Svg, { G, Rect, Path } from "react-native-svg";
const Rightarrow = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G
      id="Group_6718"
      data-name="Group 6718"
      transform="translate(997 108)"
      opacity={0.54}
    >
      <Rect
        id="Rectangle_2661"
        data-name="Rectangle 2661"
        width={24}
        height={24}
        transform="translate(-997 -108)"
        fill="none"
      />
      <Path
        id="Path_121"
        data-name="Path 121"
        d="M.289,15.568a.839.839,0,0,0,1.156,0L8.209,9.04a1.54,1.54,0,0,0,0-2.233L1.4.231A.84.84,0,0,0,.248.223.771.771,0,0,0,.239,1.348L6.475,7.365a.77.77,0,0,1,0,1.117L.289,14.451a.769.769,0,0,0,0,1.116"
        transform="translate(-989.344 -103.899)"
        fillRule="evenodd"
      />
    </G>
  </Svg>
);
export default Rightarrow;
