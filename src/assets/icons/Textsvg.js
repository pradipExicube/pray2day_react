import * as React from "react";
import Svg, { Text, TSpan } from "react-native-svg";
const Textsvg = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={147}
    height={24}
    viewBox="0 0 147 24"
    {...props}
  >
    <Text
      id="Jesus_Mary_Joseph"
      data-name="Jesus Mary Joseph"
      transform="translate(0 19)"
      fill="#3560c5"
      fontSize={18}
      fontFamily="PlayfairDisplay-Medium, Playfair Display"
      fontWeight={500}
    >
      <TSpan x={0} y={0}>
        {"Jesus "}
      </TSpan>
      <TSpan y={0} fill="#3e2513">
        {"Mary"}
      </TSpan>
      <TSpan y={0} />
      <TSpan y={0} fill="#e5782a">
        {" Joseph"}
      </TSpan>
    </Text>
  </Svg>
);
export default Textsvg;
