import * as React from "react";
import Svg, { SvgProps, Text, TSpan } from "react-native-svg";
const JesusMaryIcon = (props: SvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={238}
    height={103}
    viewBox="0 0 208 103"
    {...props}
  >
    <Text
      id="Jesus_Mary_Joseph"
      data-name="Jesus Mary Joseph"
      transform="translate(104 43)"
      fill="#fff"
      fontSize={40}
      fontFamily="PlayfairDisplay-Black, Playfair Display"
      fontWeight={800}
    >
      <TSpan x={-115} y={0}>
        {"Jesus Mary"}
      </TSpan>
      <TSpan x={-63} y={50}>
        {"Joseph"}
      </TSpan>
    </Text>
  </Svg>
);
export default JesusMaryIcon;
