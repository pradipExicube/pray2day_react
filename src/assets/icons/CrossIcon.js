import * as React from "react";
import Svg, { SvgProps, G, Rect, Path } from "react-native-svg";
const CrossIcon = (props: SvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_230" data-name="Group 230" transform="translate(-0.137)">
      <Rect
        id="Rectangle_74"
        data-name="Rectangle 74"
        width={24}
        height={24}
        transform="translate(0.137)"
        fill="none"
      />
      <Path
        id="Path_60"
        data-name="Path 60"
        d="M11.605,10.466,16.7,5.381A.812.812,0,1,0,15.55,4.234L10.466,9.326,5.381,4.234A.812.812,0,0,0,4.233,5.381l5.093,5.084L4.233,15.55A.812.812,0,1,0,5.381,16.7l5.084-5.093L15.55,16.7A.812.812,0,1,0,16.7,15.55Z"
        transform="translate(1.672 1.533)"
        fill="#0c0000"
      />
    </G>
  </Svg>
);
export default CrossIcon;
