import * as React from "react";
import Svg, { G, Rect, Path } from "react-native-svg";
const Litugry = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G
      id="Group_6419"
      data-name="Group 6419"
      transform="translate(997 108)"
      opacity={0.54}
    >
      <Rect
        id="Rectangle_2661"
        data-name="Rectangle 2661"
        width={24}
        height={24}
        transform="translate(-997 -108)"
        fill="none"
      />
      <Path
        id="Subtraction_3"
        data-name="Subtraction 3"
        d="M-1921.9-582h-10a3.9,3.9,0,0,1-3.9-3.9v-12a3.9,3.9,0,0,1,3.9-3.9h10a1.9,1.9,0,0,1,1.9,1.9v16A1.9,1.9,0,0,1-1921.9-582Zm-10-6a2.1,2.1,0,0,0-2.1,2.1,2.1,2.1,0,0,0,2.1,2.1h10.1V-588Zm0-12a2.1,2.1,0,0,0-2.1,2.1v8.736l.151-.091a3.816,3.816,0,0,1,1.949-.545h10.1V-600Zm6,4h-4a.9.9,0,0,1-.9-.9.9.9,0,0,1,.9-.9h4a.9.9,0,0,1,.9.9A.9.9,0,0,1-1925.9-596Z"
        transform="translate(942.9 495.9)"
      />
    </G>
  </Svg>
);
export default Litugry;
