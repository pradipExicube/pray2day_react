import * as React from "react";
import Svg, { SvgProps, G, Rect, Path } from "react-native-svg";
const MenuIcon = (props: SvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_230" data-name="Group 230" transform="translate(-0.137)">
      <Rect
        id="Rectangle_74"
        data-name="Rectangle 74"
        width={24}
        height={24}
        transform="translate(0.137)"
        fill="none"
      />
      <Path
        id="_9f93169563fec8d1381b15509f785555"
        data-name="9f93169563fec8d1381b15509f785555"
        d="M19.76,18.361a1.068,1.068,0,1,0,0-2.137H11.95a1.069,1.069,0,1,0,0,2.137Zm0-8.551a1.068,1.068,0,1,0,0-2.137H2.259a1.068,1.068,0,1,0,0,2.137Z"
        transform="translate(1.128 -1.017)"
        fill={props.menuIconColor ? props.menuIconColor : '#000'}
      />
    </G>
  </Svg>
);
export default MenuIcon;
