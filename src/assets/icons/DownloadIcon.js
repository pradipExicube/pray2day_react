import * as React from "react";
import Svg, { SvgProps, G, Rect, Path } from "react-native-svg";
const DownloadIcon = (props: SvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    viewBox="0 0 24 24"
    {...props}
  >
    <G id="Group_230" data-name="Group 230" transform="translate(-0.137)">
      <Rect
        id="Rectangle_74"
        data-name="Rectangle 74"
        width={24}
        height={24}
        transform="translate(0.137)"
        fill="none"
      />
      <Path
        id="Path_122"
        data-name="Path 122"
        d="M16.238,10.992a.749.749,0,0,0-.749.749v3a.749.749,0,0,1-.749.749H4.248a.749.749,0,0,1-.749-.749v-3a.749.749,0,0,0-1.5,0v3a2.248,2.248,0,0,0,2.248,2.248H14.739a2.248,2.248,0,0,0,2.248-2.248v-3A.749.749,0,0,0,16.238,10.992ZM8.962,12.274a.749.749,0,0,0,.247.157.7.7,0,0,0,.57,0,.749.749,0,0,0,.247-.157l3-3a.752.752,0,0,0-1.064-1.064L10.243,9.936V2.749a.749.749,0,1,0-1.5,0V9.936L7.028,8.212A.752.752,0,0,0,5.964,9.276Z"
        transform="translate(2.643 2.506)"
        fill="#0c0000"
      />
    </G>
  </Svg>
);
export default DownloadIcon;
