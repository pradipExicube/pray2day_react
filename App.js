
import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React, { createRef } from 'react'
import Theme from './src/Constants/Theme'
import Icon from 'react-native-vector-icons/Ionicons'
import { store } from './src/Store/AppStore'
import { Provider } from 'react-redux'
import { ThemeProvider } from './src/Constants/ThemeContext'
import { NavigationContainer } from '@react-navigation/native'
import MainNavigation from './src/Navigation/MainNavigation'
import { LogBox } from 'react-native';

export const navRef=createRef()
export const isReady=createRef()

const App = () => {
  
// LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications
  
  React.useEffect(() => {
    return () => (isReady.current = false);
  }, []);
  
    return <Provider store={store}>
    <ThemeProvider>
    <NavigationContainer onReady={()=>isReady.current=true} ref={navRef}>
      <MainNavigation />
      </NavigationContainer>
    </ThemeProvider>
  </Provider>
}

export default App

const styles = StyleSheet.create({})